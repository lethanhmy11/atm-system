﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.AtmSystem.ViewModels.Accounts
{
    public class AccountViewModel
    {
        public int AccountID { get; set; }
        public int CustID { get; set; }
        public string AccountNo { get; set; }
        public int ODID { get; set; }
        public int WDID { get; set; }
        public decimal Balance { get; set; }
        public string Name { get; set; }
    }
}
