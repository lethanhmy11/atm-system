﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.AtmSystem.ViewModels.ATMs
{
    public class ATMVM
    {
        public int ATMID { get; set; }
        public string Branch { get; set; }
        public string Address { get; set; }
        public string Image { get; set; }
        
        [NotMapped]
        public string? Token { get; set; }
    }
}
