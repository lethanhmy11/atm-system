﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FA.AtmSystem.Cards.ViewModels
{
    public class CardValidatePINVM
    {
        public string CardNo { get; set; }
        public string InputPIN { get; set; }
        [NotMapped]
        public string? Token { get; set; }
    }
}
