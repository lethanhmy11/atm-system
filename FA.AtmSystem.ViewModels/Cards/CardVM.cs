﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.AtmSystem.ViewModels.Cards
{
    public class CardVM
    {
        public string CardNo { get; set; }
        public string Status { get; set; }
        public int AccountID { get; set; }
        public string PIN { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime ExpiredDate { get; set; }
        public int Attempt { get; set; }
        public string Name { get; set; }
        public string AccountNo { get; set; }

    }
}
