﻿namespace FA.AtmSystem.ViewModels.ViewHistoryDtos
{
    public class ViewHistoryLogDto
    {
        public int LogID { get; set; }
        public int LogTypeID { get; set; }
        public int ATMID { get; set; }
        public string CardNo { get; set; }
        public DateTime LogDate { get; set; }
        public decimal Amount { get; set; }
        public string Details { get; set; }

        public ViewHistoryLogTypeDto LogType { get; set; }
        public ViewHistoryATMDto ATM { get; set; }

    }
}
