﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.AtmSystem.ViewModels.LogVMs
{
    public class LogViewModel
    {
        public string Description { get; set; }
        public DateTime LogDate { get; set; }
        public decimal Amount { get; set; }
        public string Details { get; set; }

        //public LogType LogType { get; set; }
        //public ATM ATM { get; set; }
        //public Card Card { get; set; }
    }
}
