﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.AtmSystem.ViewModels.LogVMs
{
    public class LogCreateViewModel
    {
        public int LogTypeID { get; set; }
        public int ATMID { get; set; }
        public string CardNo { get; set; }
        public DateTime LogDate { get; set; }
        public decimal Amount { get; set; }
        public string Details { get; set; }
    }
}
