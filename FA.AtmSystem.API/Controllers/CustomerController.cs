using AutoMapper;
using FA.AtmSystem.Core.Infrastructures;
using FA.AtmSystem.Core.Models;
using FA.AtmSystem.ViewModels.LogVMs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FA.AtmSystem.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "ATM,Customer")]
    public class CustomerController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CustomerController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpPost]
        [Route("WithDraw/{ATMId}/{WithdrawAmount}/{CardNo}")]
        
        public IActionResult WithDraw(int ATMId, decimal WithdrawAmount, string CardNo)
        {
            if (!_unitOfWork.AccountRepository.CheckConfigWithdraw(WithdrawAmount))
            {
                return StatusCode(500, new LogViewModel { Description = "Number you enter must be beetween 50.000 and 10.000.000. Enter your amount you want to withdraw again" });
            }
            if (Math.Round(WithdrawAmount) % 50000 != 0)
            { 
                return StatusCode(500, new LogViewModel { Description = "Number you enter must be divisible to 50.0000. Enter your amount you want to withdraw again" });
            }
            if (WithdrawAmount > _unitOfWork.StockRepository.GetAllMoney(ATMId))
            {
                return StatusCode(500, new LogViewModel { Description = "ATM not enough money. Enter less than your amount you want to withdraw again" });
            }
            bool enoughMoney = _unitOfWork.AccountRepository.CheckEnoughMoneyToWithdraw(WithdrawAmount, CardNo);
            if (enoughMoney)
            {
                bool isValid = _unitOfWork.AccountRepository.CheckWithdrawCash(WithdrawAmount, CardNo);
                if (isValid)
                {
                    //Tính toán số tờ rút ra trong stock
                    int[] take = _unitOfWork.StockRepository.ChangeWithDrawMoney(WithdrawAmount, ATMId);
                    var stock = _unitOfWork.StockRepository.Find(x => x.ATMID == ATMId).OrderByDescending(p => p.MoneyID).ToList();
                    for (int i = 0; i < take.Length; i++)
                    {
                        if (take[i] != 0)
                        {
                           // var stock = _unitOfWork.StockRepository.Find(x => x.ATMID == ATMId && x.MoneyID == i + 1).FirstOrDefault();
                            stock[i].Quantity = stock[i].Quantity - take[i];
                            _unitOfWork.StockRepository.Update(stock[i]);
                        }
                    }

                    var card = _unitOfWork.CardRepository.Find(c => c.CardNo == CardNo).FirstOrDefault();
                    //Trừ tiền trong tài khoản
                    var account = _unitOfWork.AccountRepository.Find(card.AccountID);
                    decimal newBalance = Math.Round(account.Balance) - WithdrawAmount;
                    account.Balance = newBalance;
                    _unitOfWork.AccountRepository.Update(account);
                    //Lưu giao dịch
                    Log log = new Log();
                    log.CardNo = CardNo;
                    log.ATMID = ATMId;
                    log.LogTypeID = 1;
                    log.Amount = WithdrawAmount;
                    log.LogDate = DateTime.Now;
                    log.Details = "Withdraw";
                    _unitOfWork.LogRepository.Add(log);
                    _unitOfWork.SaveChange();
                    var logVM = _mapper.Map<LogViewModel>(log);
                    return Ok(logVM);
                }
                else
                {
                    return StatusCode(500, new LogViewModel { Description = "Your account is too limited to draw a money in date. Enter your amount you want to withdraw again" });
                }
            }
            else
            {
                return StatusCode(500, new LogViewModel { Description = "Your account isn't enough money. Enter your amount you want to withdraw again" });
            }
            
        }
        [HttpPatch]
        [Route("ChangePIN/{cardNo}/{oldPIN}/{newPIN}/{rePIN}/{atmID}")]
        
        public IActionResult ChangePIN(string cardNo, string oldPIN, string newPIN, string rePIN, int atmID)
        {
            var cardObj = _unitOfWork.CardRepository.Find(c => c.CardNo == cardNo).FirstOrDefault();

            
                if (cardObj.CardNo == null)
                {
                    return BadRequest(ModelState);
                }
               
            
            
            if (oldPIN.ToString() == null && newPIN.ToString() == null && cardNo == null)
            {
                return BadRequest(ModelState);
            }

            if (oldPIN != cardObj.PIN)
            {
                ModelState.AddModelError("", "PIN OLD not correct! Please enter again!");
                return StatusCode(404, ModelState);
            }

            if (oldPIN == newPIN)
            {
                ModelState.AddModelError("", "NEW PIN and OLD PIN don't match! Please enter again!");
                return StatusCode(404, ModelState);
            }


            if (newPIN != rePIN)
            {
                ModelState.AddModelError("", "RE-PIN and NEW PIN don't match! Please enter again!");
                return StatusCode(404, ModelState);
            }

            if(newPIN.Length > 6 || rePIN.Length > 6)
            {
                ModelState.AddModelError("", "Character is 6 length! Please enter again!");
                return StatusCode(404, ModelState);
            }
            //Update PIN
            cardObj.PIN = newPIN;

            _unitOfWork.CardRepository.Update(cardObj);
            _unitOfWork.SaveChange();

            //Add Log with description "Change PIN"
            var logType = _unitOfWork.LogTypeRepository.Find(4);
            var logList = _unitOfWork.LogRepository.GetAll().LastOrDefault();

            Log log = new Log();
            log.LogTypeID = 4;
            log.ATMID = atmID;
            log.CardNo = cardNo;
            log.LogDate = DateTime.Now;
            log.Amount = 0;
            log.Details = logType.Description;

            _unitOfWork.LogRepository.Add(log);
            _unitOfWork.SaveChange();
            var logVM = _mapper.Map<LogViewModel>(log);

            return Ok(logVM);
        }

        [HttpPost]
        [Route("Transfer/{ATMId}/{WithdrawAmount}/{CardNo}/{AcountNoRecive}")]
        
        public IActionResult Transfer(int ATMId, decimal WithdrawAmount, string CardNo, string AcountNoRecive)
        {

            //Ckeck AccountNoRecive is Valid

            var balance = _unitOfWork.AccountRepository.CheckBalance(CardNo);
            if (balance >= WithdrawAmount)
            {

                var card = _unitOfWork.CardRepository.Find(c => c.CardNo == CardNo).FirstOrDefault();
                //Trừ tiền trong tài khoản chuyển
                var accountTransfer = _unitOfWork.AccountRepository.Find(card.AccountID);
                accountTransfer.Balance = accountTransfer.Balance - WithdrawAmount;
                _unitOfWork.AccountRepository.Update(accountTransfer);
                //Cộng tiền trong tài khoản nhận
                var accountRecivce = _unitOfWork.AccountRepository.Find(p => p.AccountNo == AcountNoRecive).FirstOrDefault();
                accountRecivce.Balance = accountRecivce.Balance + WithdrawAmount;
                _unitOfWork.AccountRepository.Update(accountRecivce);
                var customer = _unitOfWork.CustomerRepository.Find(x => x.CustID == accountRecivce.CustID).FirstOrDefault();
                //Lưu giao dịch
                Log log = new Log();
                log.CardNo = CardNo;
                log.ATMID = ATMId;
                log.LogTypeID = 2;
                log.Amount = WithdrawAmount;
                log.LogDate = DateTime.Now;
                log.Details = customer.Name;
                _unitOfWork.LogRepository.Add(log);
                _unitOfWork.SaveChange();
                var logVM = _mapper.Map<LogViewModel>(log);
                return Ok(logVM);
            }
            else
            {
                //ModelState.AddModelError("Error", $"Your account isn't enough to transfer");
                //return StatusCode(500, ModelState);
                return StatusCode(500, new LogViewModel { Description = "Your account isn't enough to transfer" });
            }
        }
        
    }
}
