using AutoMapper;
using FA.AtmSystem.Core.Infrastructures;
using FA.AtmSystem.ViewModels.Accounts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FA.AtmSystem.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "ATM,Customer")]
    public class AccountController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public AccountController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpPost]
        [Route("CheckAccountIsValid/{AccountNo}")]
        
        public IActionResult CheckAccountIsValid(string AccountNo)
        {
            var result=_unitOfWork.AccountRepository.CheckAccountIsValid(AccountNo);
            if (result)
            {
                return Ok();
            }
            return StatusCode(404);
        }

        [HttpPost]
        [Route("GetAccount/{AccountNo}")]
        
        public IActionResult GetAccount(string AccountNo)
        {
            var result = _unitOfWork.AccountRepository.Find(x=>x.AccountNo==AccountNo).FirstOrDefault();
            if (result!=null)
            {
                var accountViewModel = _mapper.Map<AccountViewModel>(result);
                accountViewModel.Name = _unitOfWork.CustomerRepository.Find(result.CustID).Name;
                return Ok(accountViewModel);
            }
            return StatusCode(404, new AccountViewModel());
        }

        [HttpPost]
        [Route("CheckBalance/{CardNo}")]
        
        public IActionResult CheckBalance(string CardNo)
        {
            var result = _unitOfWork.AccountRepository.CheckBalance(CardNo);
            return Ok(new AccountViewModel { Balance= result});
        }

        
    }
}
