﻿using AutoMapper;
using FA.AtmSystem.Core.Infrastructures;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FA.AtmSystem.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles ="ATM")]
    public class StockController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public StockController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpGet()]
        [Route("GetAllMoneyOfMoneyId/MoneyId&ATMId")]
        public IActionResult GetAllMoneyOfMoneyId(int MoneyId, int ATMId)
        {
            decimal amountMoney = _unitOfWork.StockRepository.GetAllMoneyOfMoneyId(MoneyId, ATMId);
            return Ok(amountMoney);
        }
    }
}
