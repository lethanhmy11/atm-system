﻿using AutoMapper;
using FA.AtmSystem.Core.Infrastructures;
using FA.AtmSystem.ViewModels.Cards;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FA.AtmSystem.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "ATM,Customer")]
    public class CardController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CardController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        [HttpPatch]
        [Route("GetCard/{cardNo}")]

        public IActionResult GetCard(string cardNo)
        {
            var result = _unitOfWork.CardRepository.Find(x => x.CardNo == cardNo).FirstOrDefault();
            if (result != null)
            {
                var cardViewModel = _mapper.Map<CardVM>(result);
                var account = _unitOfWork.AccountRepository.Find(x => x.AccountID == cardViewModel.AccountID).FirstOrDefault();
                cardViewModel.Name = _unitOfWork.CustomerRepository.Find(account.CustID).Name;
                cardViewModel.AccountNo = _unitOfWork.AccountRepository.Find(account.AccountID).AccountNo;
                return Ok(cardViewModel);
            }
            ModelState.AddModelError("", "Error!");
            return StatusCode(404,ModelState);
        }

    }
}
