using FA.AtmSystem.Cards.ViewModels;
using FA.AtmSystem.Core.IRepositories;
using FA.AtmSystem.ViewModels.ATMs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FA.AtmSystem.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "ATM")]
    [ApiController]
    public class ATMsController : ControllerBase
    {
        private readonly IATMRepository _ATMRepository;
        public ATMsController(IATMRepository ATMRepository)
        {
            _ATMRepository = ATMRepository;
        }

        [HttpPost("ValidateCard")]
        public IActionResult ValidateCard([FromBody] CardValidateVM inputCard)
        {
            var card = _ATMRepository.ValidateCard(inputCard.CardNo);
            if (card== null)
            {
                return BadRequest(new { message = "Invalid card" });
            }
            return Ok(card);
        }

        [HttpPost]
        [Route("ValidateATM/{atmId}")]
        [AllowAnonymous]
        public IActionResult ValidateATM(int atmId)
        {
           
            var atm = _ATMRepository.ValidateATM(atmId);
            if (atm == null)
            {
                return BadRequest(new { message = "Invalid atm" });
            }
            return Ok(atm);
        }

        [HttpPost("ValidatePIN")]
        public IActionResult ValidatePIN([FromBody] CardValidatePINVM inputCard)
        {
            var card = _ATMRepository.ValidatePIN(inputCard.CardNo, inputCard.InputPIN);
            if (card == null)
            {
                return BadRequest(new { message = "Invalid PIN" });
            }
            return Ok(card);
        }

        [HttpPost("ResetCardAttempt")]
        public IActionResult ResetCardAttempt([FromBody] CardValidateVM inputCard)
        {
            var card = _ATMRepository.ResetAttempt(inputCard.CardNo);
            if (card == null)
            {
                return BadRequest(new { message = "Failed to reset Card Attemp" });
            }
            return Ok(card);

        }

        [HttpPost("IncreaseCardAttempt")]
        public IActionResult IncreaseCardAttempt([FromBody] CardValidateVM inputCard)
        {
            var card = _ATMRepository.IncreaseAttempt(inputCard.CardNo);
            if (card == null)
            {
                return BadRequest(new { message = "Failed to increase Card Attemp" });
            }
            return Ok(card);

        }

        [HttpPost("SwallowCard")]
        public IActionResult SwallowCard([FromBody] CardValidateVM inputCard)
        {
            var card = _ATMRepository.SwallowCard(inputCard.CardNo);
            if (card == null)
            {
                return BadRequest(new { message = "Failed to swallow card" });
            }
            return Ok(card);

        }

        [HttpGet]
        [Route("GetATMs")]
        [AllowAnonymous]
        
        public IActionResult GetATMs()
        {
            var ATMs = _ATMRepository.GetAll();
            
            return Ok(ATMs);
        }



    }
}
