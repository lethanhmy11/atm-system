﻿using AutoMapper;
using FA.AtmSystem.Core.Infrastructures;
using FA.AtmSystem.ViewModels.ViewHistoryDtos;
using Microsoft.AspNetCore.Mvc;

namespace FA.AtmSystem.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LogController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public LogController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all transactions within the previous "duration" days
        /// 1 week: duration = 7
        /// 1 month: duration = 30
        /// 4 months: duration = 120
        /// 6 months: duration = 180
        /// 1 year: duration = 365
        /// 2 years: duration = 730
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(200, Type = typeof(List<ViewHistoryLogDto>))]
        [Route("GetAllOfHistoryTransactions/{cardNo}/{duration}/{index}")]
        public IActionResult GetAllOfHistoryTransactions(string cardNo, int duration, int index = 0)
        {
            List<ViewHistoryLogDto> objDto = GetDataWithADurationAndConvertToDto(cardNo, duration, index);
            return Ok(objDto);
        }


        /// <summary>
        /// Return DTO which is converted from taking Domain data in a previous duration 
        /// </summary>
        /// <param name="duration"></param>
        /// <returns></returns>
        private List<ViewHistoryLogDto> GetDataWithADurationAndConvertToDto(string cardNo, int duration, int index)
        {
            var objList = _unitOfWork.LogRepository.GetLogsInADurationBefore(cardNo, duration, index);
            var objDto = new List<ViewHistoryLogDto>();
            foreach (var obj in objList)
            {

                objDto.Add(_mapper.Map<ViewHistoryLogDto>(obj));
            }

            return objDto;
        }
    }
}
