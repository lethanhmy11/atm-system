﻿using FA.AtmSystem.Core;
using FA.AtmSystem.Core.Models;
using FA.AtmSystem.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace FA.AtmSystem.Tests.RepositoryTests
{
    [TestClass]
    public class AccountRepositoryTest
    {
        private AccountRepository _accountReponsitory;
        private AtmSystemContext _context;
       
        private static DbContextOptions<AtmSystemContext> options= new DbContextOptionsBuilder<AtmSystemContext>()
            .UseInMemoryDatabase(databaseName: "DataBase")
            .Options;

        [SetUp]
        public void SetUp()
        {
            
            _context = new AtmSystemContext(options);
            _context.Database.EnsureCreated();           
                _context.Cards.Add(
                   new Card()
                   {
                       CardNo = "1",
                       Status = "valid",
                       AccountID = 1,
                       PIN = "131020",
                       StartDate = new DateTime(2022, 2, 14),
                       ExpiredDate = new DateTime(2025, 2, 28),
                       Attempt = 0,
                       Account = new Account() { 
                           AccountID = 111,
                           CustID = 1,
                           AccountNo = "10716241116",
                           ODID = 100,
                           WDID = 100,
                           Balance = 10000
                       },
                   });
                _context.Cards.Add(
                  new Card()
                  {
                      CardNo = "2",
                      Status = "valid",
                      AccountID = 2,
                      PIN = "188991",
                      StartDate = new DateTime(2022, 2, 14),
                      ExpiredDate = new DateTime(2025, 2, 28),
                      Attempt = 0,
                      Account = new Account() {
                          AccountID = 222,
                          CustID = 1,
                          AccountNo = "10728451100",
                          ODID = 100,
                          WDID = 100,
                          Balance = 3000000,
                      },
                  });
                _context.Cards.Add(
                    new Card()
                    {
                        CardNo = "3",
                        Status = "invalid",
                        AccountID = 3,
                        PIN = "118116",
                        StartDate = new DateTime(2022, 2, 14),
                        ExpiredDate = new DateTime(2025, 2, 28),
                        Attempt = 0,
                        Account = new Account() { 
                            AccountID = 333,
                            CustID = 11,
                            AccountNo = "10734561196",
                            ODID = 100,
                            WDID = 100,
                            Balance = 1000000 },
                    });

            _context.OverDarfts.Add(
                new OverDraft() { ODID = 100, Value = 500000 });
            _context.WithdrawLimits.Add(
                new WithdrawLimit() { WDID = 100, Value = 2000000 });
            //_context.Configs.Add(
            //    new Config() { ConfigID = , MinWithdraw = 50000.00m, MaxWithdraw = 10000000.00m, DateModified = new DateTime(2019, 02, 28), NumPerPage = 3 }
            //);
            _context.SaveChanges();
            _accountReponsitory = new AccountRepository(_context);
        }

        [Test]
        public void CheckBalance_WhenCalled_ReturnBalanceOfAccount()
        {
            var result = _accountReponsitory.CheckBalance("1");
            Assert.AreEqual(10000, result);
        }

        [Test]
        public void CheckAccountIsValid_WhenCalled_ReturnTrue()
        {
            var result = _accountReponsitory.CheckAccountIsValid("10716241116");
            Assert.IsTrue(result);
        }

        [Test]
        public void CheckAccountIsValid_WhenCalled_ReturnFalse()
        {
            var result = _accountReponsitory.CheckAccountIsValid("10734561196");
            Assert.IsFalse(result);
        }

        [Test]
        public void CheckConfigWithdraw_WhenCalled_ReturnFalse()
        {
            var result = _accountReponsitory.CheckConfigWithdraw((10));
            Assert.IsFalse(result);
        }

        [Test]
        //Đã vượt quá số tiền được rút trong 1 ngày
        public void CheckWithdrawCash_WhenCalled_ReturnTrue()
        {
            var result = _accountReponsitory.CheckWithdrawCash(1000000,"1");
            Assert.IsTrue(result);
        }

        [Test]
        public void CheckWithdrawCash_WhenCalled_ReturnFalse()
        {
            var result = _accountReponsitory.CheckWithdrawCash(1000000000, "1");
            Assert.IsFalse(result);
        }

        [Test]
        public void CheckEnoughMoneyToWithdraw_WhenCalled_ReturnFalse()
        {
            var result = _accountReponsitory.CheckEnoughMoneyToWithdraw(1000000000, "1");
            Assert.IsFalse(result);
        }

        [Test]
        public void CheckEnoughMoneyToWithdraw_WhenCalled_ReturnTrue()
        {
            var result = _accountReponsitory.CheckEnoughMoneyToWithdraw(1000000, "2");
            Assert.IsTrue(result);
        }

        [Test]
        public void GetAll_WhenCalled_ReturnTrue()
        {
            _accountReponsitory.GetAll();
            int rowCount = _context.SaveChanges();

            Assert.AreEqual(0, rowCount);
        }

        [TearDown]
        public void DeleteContext()
        {
            _context.Database.EnsureDeleted();
        }

    }
}
