﻿using FA.AtmSystem.Core;
using FA.AtmSystem.Core.Models;
using FA.AtmSystem.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace FA.AtmSystem.Tests.RepositoryTests
{
    [TestClass]
    public class LogRepositoryTest
    {
        private LogRepository _logReponsitory;
        private AtmSystemContext _context;
        private static DbContextOptions<AtmSystemContext> options = new DbContextOptionsBuilder<AtmSystemContext>()
            .UseInMemoryDatabase(databaseName: "DataBase")
            .Options;

        [SetUp]
        public void SetUp()
        {
            _context = new AtmSystemContext(options);
            _context.Database.EnsureCreated();
            var log = new List<Log>()
            {
                new Log()
                    {
                        LogID = 1,
                        LogDate = new DateTime(2022, 01, 11),
                        Amount = 1000000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 1,
                        CardNo = "9696308315598901"
                    },
                    new Log()
                    {
                        LogID = 2,
                        LogDate = new DateTime(2022, 02, 5),
                        Amount = 2000000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 1,
                        CardNo = "9696308315598901"
                    },
                    new Log()
                    {
                        LogID = 3,
                        LogDate = new DateTime(2022, 06, 24),
                        Amount = 3000000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 1,
                        CardNo = "9696308315598901"
                    },
                    new Log()
                    {
                        LogID = 4,
                        LogDate = new DateTime(2022, 05, 20),
                        Amount = 4000000.00m,
                        Details = "Le Ngoc Hung",
                        LogTypeID = 2,
                        ATMID = 1,
                        CardNo = "2360870160342161"
                    },
                    new Log()
                    {
                        LogID = 5,
                        LogDate = new DateTime(2022, 01, 11),
                        Amount = 500000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 1,
                        CardNo = "2360870160342161"
                    },
                    new Log()
                    {
                        LogID = 6,
                        LogDate = new DateTime(2022, 12, 30),
                        Amount = 6000000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 1,
                        CardNo = "4966575087181020"
                    },
                    new Log()
                    {
                        LogID = 7,
                        LogDate = new DateTime(2022, 10, 30),
                        Amount = 3000000.00m,
                        Details = "Check balance",
                        LogTypeID = 3,
                        ATMID = 7,
                        CardNo = "2836996267348266"
                    },
                    new Log()
                    {
                        LogID = 8,
                        LogDate = new DateTime(2022, 02, 28),
                        Amount = 500000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 2,
                        CardNo = "2836996267348266"
                    },
                    new Log()
                    {
                        LogID = 9,
                        LogDate = new DateTime(2022, 02, 28),
                        Amount = 9000000.00m,
                        Details = "Le Thi Thanh My",
                        LogTypeID = 2,
                        ATMID = 2,
                        CardNo = "5539328606652179"
                    },
                    new Log()
                    {
                        LogID = 10,
                        LogDate = new DateTime(2022, 07, 01),
                        Amount = 0,
                        Details = "Check balance",
                        LogTypeID = 3,
                        ATMID = 2,
                        CardNo = "5539328606652179"
                    },
                    new Log()
                    {
                        LogID = 11,
                        LogDate = new DateTime(2022, 03, 28),
                        Amount = 1000000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 3,
                        CardNo = "0993755230042035"
                    },
                    new Log()
                    {
                        LogID = 12,
                        LogDate = new DateTime(2022, 05, 01),
                        Amount = 5000000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 2,
                        CardNo = "8821491318413200"
                    },
                    new Log()
                    {
                        LogID = 13,
                        LogDate = new DateTime(2022, 02, 12),
                        Amount = 1000000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 2,
                        CardNo = "6835760905033599"
                    },
                    new Log()
                    {
                        LogID = 14,
                        LogDate = new DateTime(2022, 05, 12),
                        Amount = 2000000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 2,
                        CardNo = "1682200004184550"
                    },
                    new Log()
                    {
                        LogID = 15,
                        LogDate = new DateTime(2022, 03, 28),
                        Amount = 2000000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 2,
                        CardNo = "1282325404183010"
                    },
                    new Log()
                    {
                        LogID = 16,
                        LogDate = new DateTime(2022, 05, 05),
                        Amount = 5000000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 2,
                        CardNo = "1282325404183010"
                    },
                    new Log()
                    {
                        LogID = 17,
                        LogDate = new DateTime(2022, 02, 08),
                        Amount = 1000000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 3,
                        CardNo = "7084054246868173"
                    },
                    new Log()
                    {
                        LogID = 18,
                        LogDate = new DateTime(2022, 03, 28),
                        Amount = 2000000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 3,
                        CardNo = "9084147846861734"
                    },
                    new Log()
                    {
                        LogID = 19,
                        LogDate = new DateTime(2022, 04, 01),
                        Amount = 2000000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 3,
                        CardNo = "0115755230042035"
                    },
                    new Log()
                    {
                        LogID = 20,
                        LogDate = new DateTime(2022, 04, 12),
                        Amount = 5000000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 3,
                        CardNo = "0115755230042035"
                    },
                    new Log()
                    {
                        LogID = 21,
                        LogDate = new DateTime(2022, 02, 22),
                        Amount = 1000000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 3,
                        CardNo = "5539328606652179"
                    },
                    new Log()
                    {
                        LogID = 22,
                        LogDate = new DateTime(2022, 02, 23),
                        Amount = 1500000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 3,
                        CardNo = "5539328606652179"
                    }
            };
            _context.SaveChanges();
            _logReponsitory = new LogRepository(_context);
        }
       
        [Test]
        public void GetLogsInAYearBefore_WhenCalled_ReturnListLog()
        {
            var result = _logReponsitory.GetLogsInADurationBefore("9696308315598901", 365, 0);
            Assert.AreEqual(result.Count, 3);
        }

        [Test]
        public void GetLogsInAWeakBefore_WhenCalled_ReturnListLog()
        {
            var result = _logReponsitory.GetLogsInADurationBefore("9696308315598901", 7, 0);
            Assert.AreEqual(result.Count, 1);
        }


        [Test]
        public void Add_WhenCalled_ReturnTrue()
        {
            _logReponsitory.Add(new Log()
            {
                LogID = 23,
                LogDate = new DateTime(2022, 02, 23),
                Amount = 1500000.00m,
                Details = "Withdraw",
                LogTypeID = 1,
                ATMID = 5,
                CardNo = "5539328606652179"
            });

            int rowCount = _context.SaveChanges();

            Assert.AreEqual(1, rowCount);
        }

        [TearDown]
        public void DeleteContext()
        {
            _context.Database.EnsureDeleted();
        }
    }
}
