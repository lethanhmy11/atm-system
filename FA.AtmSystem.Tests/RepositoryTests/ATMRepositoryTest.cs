﻿using FA.AtmSystem.Core;
using FA.AtmSystem.Core.Models;
using FA.AtmSystem.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using System;
using System.Collections.Generic;


namespace FA.AtmSystem.Tests.RepositoryTests
{
    [TestClass]
    public class ATMRepositoryTest
    {
        private ATMRepository _atmReponsitory;
        
        private AtmSystemContext _context;
        private static DbContextOptions<AtmSystemContext> options = new DbContextOptionsBuilder<AtmSystemContext>()
            .UseInMemoryDatabase(databaseName: "DataBase")
            .Options;

        [SetUp]
        public void SetUp()
        {
            _context = new AtmSystemContext(options);
            _context.Database.EnsureCreated();
            _context.Cards.Add(
               new Card()
               {
                   CardNo = "1",
                   Status = "valid",
                   AccountID = 1,
                   PIN = "131020",
                   StartDate = new DateTime(2022, 2, 14),
                   ExpiredDate = new DateTime(2025, 2, 28),
                   Attempt = 0,
                   Account = new Account()
                   {
                       AccountID = 111,
                       CustID = 1,
                       AccountNo = "10716241116",
                       ODID = 100,
                       WDID = 100,
                       Balance = 10000
                   },
               });
            _context.Cards.Add(
              new Card()
              {
                  CardNo = "2",
                  Status = "valid",
                  AccountID = 2,
                  PIN = "188991",
                  StartDate = new DateTime(2022, 2, 14),
                  ExpiredDate = new DateTime(2025, 2, 28),
                  Attempt = 2,
                  Account = new Account()
                  {
                      AccountID = 222,
                      CustID = 1,
                      AccountNo = "10728451100",
                      ODID = 100,
                      WDID = 100,
                      Balance = 3000000,
                  },
              });

            _context.Cards.Add(
              new Card()
              {
                  CardNo = "3",
                  Status = "invalid",
                  AccountID = 2,
                  PIN = "188991",
                  StartDate = new DateTime(2022, 2, 14),
                  ExpiredDate = new DateTime(2025, 2, 28),
                  Attempt = 2,
                  Account = new Account()
                  {
                      AccountID = 333,
                      CustID = 1,
                      AccountNo = "107284511111",
                      ODID = 100,
                      WDID = 100,
                      Balance = 3000000,
                  },
              });

            _context.ATMs.Add(
                new ATM() { ATMID = 11, Branch = "Vietcomnbank", Address = "129 Đ. Lê Duẩn, Cửa Nam, Hoàn Kiếm, Hà Nội", Image = "Vietcombank.webp" });

            _context.ATMs.Add(
                 new ATM() { ATMID = 12, Branch = "Techcomnbank", Address = "461, Phố Đội Cấn, Phường Vĩnh Phúc, Quận Ba Đình, Vĩnh Phúc, Ba Đình, Hà Nội", Image = "Techcombank.webp" });


            _context.ATMs.Add(
                new ATM() { ATMID = 13, Branch = "MilitaryBank", Address = "185, Đường Hoàng Hoa Thám, Phường Liễu Giai, Quận Ba Đình, Thuỵ Khuê, Tây Hồ, Hà Nội", Image = "Militarybank.png" });

            _context.SaveChanges();           
            _atmReponsitory = new ATMRepository(_context);
        }

        [Test]
        public void IncreaseAttempt_WhenCalled_AttempIncrease1()
        {
            var result = _atmReponsitory.IncreaseAttempt("1");
            Assert.AreEqual(1, result.Attempt); 
        }

        [Test]
        public void ResetAttempt_WhenCalled_AttempResetIs0()
        {
            var result = _atmReponsitory.ResetAttempt("2");
            Assert.AreEqual(0, result.Attempt);
        }

        [Test]
        public void SwallowCard_WhenCalled_CardStatusChangeIsBlock()
        {
            var result = _atmReponsitory.SwallowCard("2");
            Assert.AreEqual("block", result.Status);
        }

        [Test]
        public void ValidateCard_WhenCalled_ReturnCard()
        {
            var result = _atmReponsitory.ValidateCard("2");
            Assert.AreEqual("2", result.CardNo);
        }

        [Test]
        public void ValidateCard_WhenCalled_ReturnNull()
        {
            var result = _atmReponsitory.ValidateCard("3");
            Assert.IsNull(result);
        }

        [Test]
        public void GetAll_WhenCalled_ReturnTrue()
        {
            _atmReponsitory.GetAll();
            int rowCount = _context.SaveChanges();

            Assert.AreEqual(0, rowCount);
        }

        [TearDown]
        public void DeleteContext()
        {
            _context.Database.EnsureDeleted();
        }
    }
}