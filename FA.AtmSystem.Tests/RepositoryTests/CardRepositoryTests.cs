﻿using FA.AtmSystem.Core;
using FA.AtmSystem.Core.Infrastructures;
using FA.AtmSystem.Core.Models;
using FA.AtmSystem.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Serialization;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.AtmSystem.Tests.RepositoryTests
{
    
    public class CardRepositoryTests
    {
        private CardRepository _cardRepository;
        private AtmSystemContext _context;
        private static DbContextOptions<AtmSystemContext> options = new DbContextOptionsBuilder<AtmSystemContext>()
            .UseInMemoryDatabase(databaseName: "DataBase")
            .Options;

        [SetUp]
        public void SetUp()
        {
            _context = new AtmSystemContext(options);
            _context.Database.EnsureCreated();
            _context.Cards.Add(
               new Card()
               {
                   CardNo = "1",
                   Status = "valid",
                   AccountID = 1,
                   PIN = "131020",
                   StartDate = new DateTime(2022, 2, 14),
                   ExpiredDate = new DateTime(2025, 2, 28),
                   Attempt = 0,
                   Account = new Account()
                   {
                       AccountID = 111,
                       CustID = 1,
                       AccountNo = "10716241116",
                       ODID = 100,
                       WDID = 100,
                       Balance = 10000
                   },
               });
            _context.Cards.Add(
              new Card()
              {
                  CardNo = "2",
                  Status = "valid",
                  AccountID = 2,
                  PIN = "188991",
                  StartDate = new DateTime(2022, 2, 14),
                  ExpiredDate = new DateTime(2025, 2, 28),
                  Attempt = 2,
                  Account = new Account()
                  {
                      AccountID = 222,
                      CustID = 1,
                      AccountNo = "10728451100",
                      ODID = 100,
                      WDID = 100,
                      Balance = 3000000,
                  },
              });

            _context.Cards.Add(
              new Card()
              {
                  CardNo = "3",
                  Status = "invalid",
                  AccountID = 2,
                  PIN = "188991",
                  StartDate = new DateTime(2022, 2, 14),
                  ExpiredDate = new DateTime(2025, 2, 28),
                  Attempt = 2,
                  Account = new Account()
                  {
                      AccountID = 333,
                      CustID = 1,
                      AccountNo = "107284511111",
                      ODID = 100,
                      WDID = 100,
                      Balance = 3000000,
                  },
              });
            _context.SaveChanges();
            _cardRepository = new CardRepository(_context);
        }


        [Test]
        public void Add_WhenCalled_ReturnCard()
        {
           
            _cardRepository.Add(new Card()
            {
                CardNo = "4",
                Status = "invalid",
                AccountID = 2,
                PIN = "115234",
                StartDate = new DateTime(2022, 2, 14),
                ExpiredDate = new DateTime(2025, 2, 28),
                Attempt = 2,
               
            });
            int rowCount = _context.SaveChanges();

            var card = _cardRepository.Find(c => c.CardNo == "4").FirstOrDefault();
            Assert.IsNotNull(card.CardNo);
            Assert.That(rowCount, Is.EqualTo(1));
        }

        [Test]
        public void Update_WhenCalled_ReturnCard()
        {
            var card = _cardRepository.Find(c => c.CardNo == "3").FirstOrDefault();
            
            card.Status = "block";
            card.Attempt = 3;
            card.PIN = "117241";
            _cardRepository.Update(card);

            int rowCount = _context.SaveChanges();

            Assert.That(rowCount, Is.EqualTo(1));

        }

        [Test]
        public void Find_WhenCalled_ReturnCard()
        {
            var card = _cardRepository.Find(c => c.CardNo == "3").FirstOrDefault();

          
            Assert.IsNotNull(card.CardNo);

        }

        [Test]
        public void Find_WhenCalledCardNoNull_ReturnNull()
        {
            var card = _cardRepository.Find(c => c.CardNo == "366").FirstOrDefault();
            Assert.IsNull(card);

        }

        [Test]
        public void GetAll_WhenCalled_ReturnTrue()
        {
            _cardRepository.GetAll();
            int rowCount = _context.SaveChanges();

            Assert.That(rowCount, Is.EqualTo(0));

        }

        [Test]
        public void Delete_WhenCalled_ReturnTrue()
        {
            var card = _cardRepository.Find(c => c.CardNo == "3").FirstOrDefault();
            _cardRepository.Delete(card);
            int rowCount = _context.SaveChanges();
            var cardObj = _cardRepository.Find(c => c.CardNo == "3").FirstOrDefault();
            Assert.IsNull(cardObj);
            Assert.IsNotNull(card);
            Assert.That(rowCount, Is.EqualTo(1));
        }

        [TearDown]
        public void DeleteContext()
        {
            _context.Database.EnsureDeleted();
        }
    }

}
