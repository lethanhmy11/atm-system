﻿using FA.AtmSystem.Core;
using FA.AtmSystem.Core.Models;
using FA.AtmSystem.Core.Repositories;
using Microsoft.EntityFrameworkCore;

using NUnit.Framework;
using System;
using System.Collections.Generic;

//using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace FA.AtmSystem.Tests.RepositoryTests
{
    [TestFixture]
    public class StockRepositoryTest
    {
        private StockRepository _stockReponsitory;
        private AtmSystemContext _context;
        private static DbContextOptions<AtmSystemContext> options = new DbContextOptionsBuilder<AtmSystemContext>()
            .UseInMemoryDatabase(databaseName: "DataBase")
            .Options;

        [SetUp]
        public void SetUp()
        {
            _context = new AtmSystemContext(options);
            _context.Database.EnsureCreated();
            _context.ATMs.Add(
                new ATM()
                {
                    ATMID = 0, Branch = "TestBank", Address = "Pullman Hà Nội 40", Image = "TestBank.png" 
                });
            List<Stock> stocks = new List<Stock>(){
               new Stock() { StockID = 30, Quantity = 5, MoneyID = 1, ATMID = 0 },
               new Stock() { StockID = 31, Quantity = 5, MoneyID = 2, ATMID = 0 },
               new Stock() { StockID = 32, Quantity = 5, MoneyID = 3, ATMID = 0 },
               new Stock() { StockID = 33, Quantity = 5, MoneyID = 4, ATMID = 0 },
           };
           
            _context.Stocks.AddRange(stocks);
            _context.SaveChanges();
            _stockReponsitory = new StockRepository(_context);
        }

        [Test]
        public void ChangeWithDrawMoney_WhenCalled_ReturnArrayNumberOfEachTypeMoneyWithdraw()
        {
            var result=_stockReponsitory.ChangeWithDrawMoney(1750000, 0);
            int[] expected = { 3, 1, 0, 1 };
            Assert.That(result, Is.EquivalentTo(expected));
            
        }

        [Test]
        public void GetAllMoney_WhenCalled_ReturnSumMoneyOfAMT()
        {
            var result = _stockReponsitory.GetAllMoney(0);
            Assert.AreEqual(4250000, result);

        }

        [Test]
        [TestCase(1, 0, 250000)]
        [TestCase(2, 0, 500000)]
        [TestCase(3, 0, 1000000)]
        [TestCase(4, 0, 2500000)]
        public void GetAllMoneyOfMoneyId_WhenCalled_ReturnSumOfOneMoneyTypeInAMT(int MoneyId, int ATMId, decimal expectResult)
        {
            var result = _stockReponsitory.GetAllMoneyOfMoneyId(MoneyId, ATMId);
            Assert.AreEqual(expectResult, result);

        }

        [TearDown]
        public void DeleteContext()
        {
            _context.Database.EnsureDeleted();
        }
    }
    
}
