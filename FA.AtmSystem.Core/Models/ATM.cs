﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FA.AtmSystem.Core.Models
{
    public class ATM
    {
        [Key]
        public int ATMID { get; set; }
        public string Branch { get; set; }
        public string Address { get; set; }
        public string Image { get; set; }
        public ICollection<Log> Logs { get; set; }
        public ICollection<Stock> Stocks { get; set; }

        [NotMapped]
        public string? Token { get; set; }
    }
}
