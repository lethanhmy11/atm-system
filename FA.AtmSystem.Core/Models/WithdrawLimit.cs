﻿using System.ComponentModel.DataAnnotations;

namespace FA.AtmSystem.Core.Models
{
    public class WithdrawLimit
    {
        [Key]
        public int WDID { get; set; }
        public decimal Value { get; set; }

        public ICollection<Account> Accounts { get; set; }
    }
}
