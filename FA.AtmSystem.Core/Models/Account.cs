﻿using System.ComponentModel.DataAnnotations;

namespace FA.AtmSystem.Core.Models
{
    public class Account
    {
        [Key]
        public int AccountID { get; set; }
        public int CustID { get; set; }
        public string AccountNo { get; set; }
        public int ODID { get; set; }
        public int WDID { get; set; }
        public decimal Balance { get; set; }

        public Customer Customer { get; set; }
        public OverDraft OverDraft { get; set; }
        public WithdrawLimit WithdrawLimit { get; set; }
        public ICollection<Card> Cards { get; set; }
    }
}
