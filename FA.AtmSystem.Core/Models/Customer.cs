﻿namespace FA.AtmSystem.Core.Models
{
    public class Customer
    {
        public int CustID { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Addr { get; set; }

        public ICollection<Account> Accounts { get; set; }
    }
}
