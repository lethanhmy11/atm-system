﻿using System.ComponentModel.DataAnnotations;

namespace FA.AtmSystem.Core.Models
{
    public class Config
    {
        [Key]
        public int ConfigID { get; set; }
        public DateTime DateModified { get; set; }
        public decimal MinWithdraw { get; set; }
        public decimal MaxWithdraw { get; set; }
        public int NumPerPage { get; set; }
    }
}
