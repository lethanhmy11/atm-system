﻿using System.ComponentModel.DataAnnotations;

namespace FA.AtmSystem.Core.Models
{
    public class Money
    {
        [Key]
        public int MoneyID { get; set; }
        public decimal MoneyValue { get; set; }

        public ICollection<Stock> Stocks { get; set; }
    }
}
