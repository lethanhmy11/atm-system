﻿using System.ComponentModel.DataAnnotations;

namespace FA.AtmSystem.Core.Models
{
    public class LogType
    {
        [Key]
        public int LogTypeId { get; set; }
        public string Description { get; set; }
        public ICollection<Log> Logs { get; set; }
    }
}
