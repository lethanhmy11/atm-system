﻿using System.ComponentModel.DataAnnotations;

namespace FA.AtmSystem.Core.Models
{
    public class Stock
    {
        [Key]
        public int StockID { get; set; }
        public int MoneyID { get; set; }
        public int ATMID { get; set; }
        public int Quantity { get; set; }

        public Money Money { get; set; }
        public ATM ATM { get; set; }
    }
}
