﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FA.AtmSystem.Core.Models
{
    public class Card
    {
        [Key]
        public string CardNo { get; set; }
        public string Status { get; set; }
        public int AccountID { get; set; }
        public string PIN { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime ExpiredDate { get; set; }
        public int Attempt { get; set; }

        public Account Account { get; set; }
        public ICollection<Log> Logs { get; set; }

        [NotMapped] 
        public string? Token { get; set; }
    }
}
