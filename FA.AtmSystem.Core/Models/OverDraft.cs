﻿using System.ComponentModel.DataAnnotations;

namespace FA.AtmSystem.Core.Models
{
    public class OverDraft
    {
        [Key]
        public int ODID { get; set; }
        public decimal Value { get; set; }

        public ICollection<Account> Accounts { get; set; }
    }
}
