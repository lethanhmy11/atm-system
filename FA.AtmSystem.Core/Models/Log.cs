﻿using System.ComponentModel.DataAnnotations;

namespace FA.AtmSystem.Core.Models
{
    public class Log
    {
        [Key]
        public int LogID { get; set; }
        public int LogTypeID { get; set; }
        public int ATMID { get; set; }
        public string CardNo { get; set; }
        public DateTime LogDate { get; set; }
        public decimal Amount { get; set; }
        public string Details { get; set; }

        public LogType LogType { get; set; }
        public ATM ATM { get; set; }
        public Card Card { get; set; }
    }
}
