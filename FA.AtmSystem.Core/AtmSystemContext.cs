﻿using FA.AtmSystem.Core.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FA.AtmSystem.Core
{
    public class AtmSystemContext : IdentityDbContext
    {
        public AtmSystemContext()
        {

        }

        public AtmSystemContext(DbContextOptions<AtmSystemContext> options) : base(options)
        {

        }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<OverDraft> OverDarfts { get; set; }
        public DbSet<WithdrawLimit> WithdrawLimits { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Card> Cards { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<ATM> ATMs { get; set; }
        public DbSet<Stock> Stocks { get; set; }
        public DbSet<Money> Moneys { get; set; }
        public DbSet<LogType> LogTypes { get; set; }
        public DbSet<Config> Configs { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            if (!optionsBuilder.IsConfigured)
            {
                string connectionString = "Server=.;Database=FA.AtmSystem;Trusted_Connection=True;";
                optionsBuilder.UseSqlServer(connectionString);
            }
        }

        protected override void ConfigureConventions(ModelConfigurationBuilder configurationBuilder)
        {
            base.ConfigureConventions(configurationBuilder);
            configurationBuilder.Properties<decimal>().HavePrecision(18, 6);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            // Config Customers
            modelBuilder.Entity<Customer>(customer =>
            {
                customer.HasKey(c => c.CustID);
                customer.Property(c => c.Name).HasColumnType("nvarchar(100)");
                customer.Property(c => c.Phone).HasColumnType("varchar(50)");
                customer.Property(c => c.Email).HasColumnType("varchar(100)");
                customer.Property(c => c.Addr).HasColumnType("nvarchar(200)");
                customer.HasMany(c => c.Accounts)
                        .WithOne(a => a.Customer)
                        .HasForeignKey(a => a.CustID);
            });

            // Config OverDrafts
            modelBuilder.Entity<OverDraft>(overDraft =>
            {
                overDraft.HasMany(o => o.Accounts)
                        .WithOne(a => a.OverDraft)
                        .HasForeignKey(a => a.ODID);
            });

            // Config WithdrawLimits
            modelBuilder.Entity<WithdrawLimit>(withdrawLimit =>
            {
                withdrawLimit.HasMany(o => o.Accounts)
                        .WithOne(a => a.WithdrawLimit)
                        .HasForeignKey(a => a.WDID);
            });

            // Config Accounts
            modelBuilder.Entity<Account>(account =>
            {
                account.Property(a => a.AccountNo).HasColumnType("varchar(50)");
                account.HasMany(a => a.Cards)
                        .WithOne(c => c.Account)
                        .HasForeignKey(c => c.AccountID);
            });

            // Config Cards
            modelBuilder.Entity<Card>(card =>
            {
                card.Property(c => c.CardNo).HasColumnType("varchar(16)");
                card.Property(c => c.Status).HasColumnType("varchar(30)");
                card.Property(c => c.PIN).HasColumnType("varchar(6)");
                card.HasMany(c => c.Logs)
                        .WithOne(l => l.Card)
                        .HasForeignKey(l => l.CardNo);
            });

            // Config ATMs
            modelBuilder.Entity<ATM>(atm =>
            {
                atm.Property(a => a.Branch).HasColumnType("varchar(50)");
                atm.Property(a => a.Address).HasColumnType("nvarchar(100)");
                atm.Property(a => a.Image).HasColumnType("nvarchar(255)");
                atm.HasMany(a => a.Stocks)
                        .WithOne(s => s.ATM)
                        .HasForeignKey(s => s.ATMID);
                atm.HasMany(a => a.Logs)
                        .WithOne(l => l.ATM)
                        .HasForeignKey(l => l.ATMID);
            });

            // Config Moneys
            modelBuilder.Entity<Money>(money =>
            {
                money.HasMany(m => m.Stocks)
                        .WithOne(s => s.Money)
                        .HasForeignKey(s => s.MoneyID);
            });

            // Config Stocks
            modelBuilder.Entity<Stock>(stock =>
            {
            });

            // Config LogTypes
            modelBuilder.Entity<LogType>(logType =>
            {
                logType.Property(l => l.Description).HasColumnType("nvarchar(100)");
                logType.HasMany(l => l.Logs)
                        .WithOne(lg => lg.LogType)
                        .HasForeignKey(lg => lg.LogTypeID);
            });

            // Config Logs
            modelBuilder.Entity<Log>(log =>
            {

                log.Property(l => l.CardNo).HasColumnType("varchar(16)");
                log.Property(l => l.Details).HasColumnType("varchar(100)");
            });

            // Config Configs
            modelBuilder.Entity<Config>(config =>
            {
            });

            modelBuilder.SeedData();
        }
    }
}
