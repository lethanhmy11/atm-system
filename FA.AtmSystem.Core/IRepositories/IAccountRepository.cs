﻿using FA.AtmSystem.Core.Infrastructures;
using FA.AtmSystem.Core.Models;

namespace FA.AtmSystem.Core.IRepositories
{
    public interface IAccountRepository : IGenericRepository<Account>
    {
        //Kiểm tra xem người dùng đã vượt quá hạn mức rút tiền trong 1 ngày. WithdrawAmount là số tiền muốn rút
        bool CheckWithdrawCash(decimal WithdrawAmount, string CardNo);

        //Kiểm tra xem người dùng có đủ tiền. WithdrawAmount là số tiền muốn rút
        bool CheckEnoughMoneyToWithdraw(decimal WithdrawAmount, string CardNo);

        //Kiểm tra xem người dùng có rút trong khoảng min-max config withdraw
        bool CheckConfigWithdraw(decimal WithdrawAmount);

        //Kiểm tra tổng tiền trong tài khoản
        decimal CheckBalance(string CardNo);

        bool CheckAccountIsValid(string AccountNo);


    }
}
