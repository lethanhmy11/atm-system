﻿using FA.AtmSystem.Core.Infrastructures;
using FA.AtmSystem.Core.Models;

namespace FA.AtmSystem.Core.IRepositories
{
    public interface IStockRepository : IGenericRepository<Stock>
    {
        //Tổng số tiền tờ 50.000 trong ATM có Id=ATMId
        decimal GetAllMoneyOfMoneyId(int MoneyId, int ATMId);
        int[] ChangeWithDrawMoney(decimal WithdrawAmount, int ATMId);
        decimal GetAllMoney(int ATMId);
    }
}
