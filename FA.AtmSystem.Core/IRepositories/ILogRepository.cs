﻿using FA.AtmSystem.Core.Infrastructures;
using FA.AtmSystem.Core.Models;

namespace FA.AtmSystem.Core.IRepositories
{
    public interface ILogRepository : IGenericRepository<Log>
    {
        ICollection<Log> GetLogsInADurationBefore(string cardNo, int duration, int index);
    }
}
