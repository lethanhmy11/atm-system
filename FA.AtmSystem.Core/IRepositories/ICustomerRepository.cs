﻿using FA.AtmSystem.Core.Infrastructures;
using FA.AtmSystem.Core.Models;

namespace FA.AtmSystem.Core.IRepositories
{
    public interface ICustomerRepository : IGenericRepository<Customer>
    {
    }
}
