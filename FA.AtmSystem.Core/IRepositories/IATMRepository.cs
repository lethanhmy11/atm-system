﻿using FA.AtmSystem.Core.Infrastructures;
using FA.AtmSystem.Core.Models;

namespace FA.AtmSystem.Core.IRepositories
{
    public interface IATMRepository : IGenericRepository<ATM>
    {
        Card ValidateCard(string cardNumber);
        ATM ValidateATM(int ATMId);
        Card ValidatePIN(string cardNo, string inputPIN);
        Card ResetAttempt(string cardNo);
        Card IncreaseAttempt(string cardNo);
        Card SwallowCard(string cardNo);
    }
}
