﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FA.AtmSystem.Core.Migrations
{
    public partial class InitDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    UserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    PasswordHash = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SecurityStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "bit", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "bit", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ATMs",
                columns: table => new
                {
                    ATMID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Branch = table.Column<string>(type: "varchar(50)", nullable: true),
                    Address = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    Image = table.Column<string>(type: "nvarchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ATMs", x => x.ATMID);
                });

            migrationBuilder.CreateTable(
                name: "Configs",
                columns: table => new
                {
                    ConfigID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateModified = table.Column<DateTime>(type: "datetime2", nullable: false),
                    MinWithdraw = table.Column<decimal>(type: "decimal(18,6)", precision: 18, scale: 6, nullable: false),
                    MaxWithdraw = table.Column<decimal>(type: "decimal(18,6)", precision: 18, scale: 6, nullable: false),
                    NumPerPage = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Configs", x => x.ConfigID);
                });

            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    CustID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    Phone = table.Column<string>(type: "varchar(50)", nullable: true),
                    Email = table.Column<string>(type: "varchar(100)", nullable: true),
                    Addr = table.Column<string>(type: "nvarchar(200)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.CustID);
                });

            migrationBuilder.CreateTable(
                name: "LogTypes",
                columns: table => new
                {
                    LogTypeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(type: "nvarchar(100)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LogTypes", x => x.LogTypeId);
                });

            migrationBuilder.CreateTable(
                name: "Moneys",
                columns: table => new
                {
                    MoneyID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MoneyValue = table.Column<decimal>(type: "decimal(18,6)", precision: 18, scale: 6, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Moneys", x => x.MoneyID);
                });

            migrationBuilder.CreateTable(
                name: "OverDarfts",
                columns: table => new
                {
                    ODID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<decimal>(type: "decimal(18,6)", precision: 18, scale: 6, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OverDarfts", x => x.ODID);
                });

            migrationBuilder.CreateTable(
                name: "WithdrawLimits",
                columns: table => new
                {
                    WDID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<decimal>(type: "decimal(18,6)", precision: 18, scale: 6, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WithdrawLimits", x => x.WDID);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderKey = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Stocks",
                columns: table => new
                {
                    StockID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MoneyID = table.Column<int>(type: "int", nullable: false),
                    ATMID = table.Column<int>(type: "int", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stocks", x => x.StockID);
                    table.ForeignKey(
                        name: "FK_Stocks_ATMs_ATMID",
                        column: x => x.ATMID,
                        principalTable: "ATMs",
                        principalColumn: "ATMID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Stocks_Moneys_MoneyID",
                        column: x => x.MoneyID,
                        principalTable: "Moneys",
                        principalColumn: "MoneyID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Accounts",
                columns: table => new
                {
                    AccountID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CustID = table.Column<int>(type: "int", nullable: false),
                    AccountNo = table.Column<string>(type: "varchar(50)", nullable: true),
                    ODID = table.Column<int>(type: "int", nullable: false),
                    WDID = table.Column<int>(type: "int", nullable: false),
                    Balance = table.Column<decimal>(type: "decimal(18,6)", precision: 18, scale: 6, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounts", x => x.AccountID);
                    table.ForeignKey(
                        name: "FK_Accounts_Customers_CustID",
                        column: x => x.CustID,
                        principalTable: "Customers",
                        principalColumn: "CustID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Accounts_OverDarfts_ODID",
                        column: x => x.ODID,
                        principalTable: "OverDarfts",
                        principalColumn: "ODID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Accounts_WithdrawLimits_WDID",
                        column: x => x.WDID,
                        principalTable: "WithdrawLimits",
                        principalColumn: "WDID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Cards",
                columns: table => new
                {
                    CardNo = table.Column<string>(type: "varchar(16)", nullable: false),
                    Status = table.Column<string>(type: "varchar(30)", nullable: true),
                    AccountID = table.Column<int>(type: "int", nullable: false),
                    PIN = table.Column<string>(type: "varchar(6)", nullable: true),
                    StartDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ExpiredDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Attempt = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cards", x => x.CardNo);
                    table.ForeignKey(
                        name: "FK_Cards_Accounts_AccountID",
                        column: x => x.AccountID,
                        principalTable: "Accounts",
                        principalColumn: "AccountID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Logs",
                columns: table => new
                {
                    LogID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LogTypeID = table.Column<int>(type: "int", nullable: false),
                    ATMID = table.Column<int>(type: "int", nullable: false),
                    CardNo = table.Column<string>(type: "varchar(16)", nullable: true),
                    LogDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(18,6)", precision: 18, scale: 6, nullable: false),
                    Details = table.Column<string>(type: "varchar(100)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Logs", x => x.LogID);
                    table.ForeignKey(
                        name: "FK_Logs_ATMs_ATMID",
                        column: x => x.ATMID,
                        principalTable: "ATMs",
                        principalColumn: "ATMID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Logs_Cards_CardNo",
                        column: x => x.CardNo,
                        principalTable: "Cards",
                        principalColumn: "CardNo");
                    table.ForeignKey(
                        name: "FK_Logs_LogTypes_LogTypeID",
                        column: x => x.LogTypeID,
                        principalTable: "LogTypes",
                        principalColumn: "LogTypeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "ATMs",
                columns: new[] { "ATMID", "Address", "Branch", "Image" },
                values: new object[,]
                {
                    { 1, "129 Đ. Lê Duẩn, Cửa Nam, Hoàn Kiếm, Hà Nội", "Vietcomnbank", "Vietcombank.webp" },
                    { 2, "461, Phố Đội Cấn, Phường Vĩnh Phúc, Quận Ba Đình, Vĩnh Phúc, Ba Đình, Hà Nội", "Techcomnbank", "Techcombank.webp" },
                    { 3, "185, Đường Hoàng Hoa Thám, Phường Liễu Giai, Quận Ba Đình, Thuỵ Khuê, Tây Hồ, Hà Nội", "MilitaryBank", "Militarybank.png" },
                    { 4, "152, Đường Thụy Khuê, Phường Thụy Khuê, Quận Tây Hồ, Thuỵ Khuê, Tây Hồ, Hà Nội", "TienPhongBank", "TPBank.webp" },
                    { 5, "285 Đội Cấn, Liễu Giai, Ba Đình, Hà Nội", "DongABank", "DongABank.webp" },
                    { 6, "349 Đội Cấn, Liễu Giai, Ba Đình, Hà Nội", "SacomBank", "Sacombank.webp" },
                    { 7, "235 Đ. Lạc Long Quân, Bái  n, Cầu Giấy, Hà Nội", "SaigonBank", "SaiGonBank.png" },
                    { 8, "3 P. Liễu Giai, Liễu Giai, Ba Đình, Hà Nội", "AgriBank", "AGriBank.webp" },
                    { 9, "Số 39, Phố Đào Tấn, Phường Cống Vị, Quận Ba Đình, Ngọc Khánh, Ba Đình, Hà Nội", "SouthernBank", "SouthernBank.png" },
                    { 10, "Pullman Hà Nội 40", "CitiBank", "CiTiBank.png" }
                });

            migrationBuilder.InsertData(
                table: "Configs",
                columns: new[] { "ConfigID", "DateModified", "MaxWithdraw", "MinWithdraw", "NumPerPage" },
                values: new object[] { 1, new DateTime(2019, 2, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), 10000000.00m, 50000.00m, 3 });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "CustID", "Addr", "Email", "Name", "Phone" },
                values: new object[,]
                {
                    { 1, "Tiên Lữ-Hưng Yên", "nam11@gmail.com", "Trần Phương Nam", "0362189562" },
                    { 2, "Long Biên-Hà Nội", "yenn12@gmail.com", "Nguyễn Thị Yến", "033562345" },
                    { 3, "Long Biên-Hà Nội", "chinh13@gmail.com", "Phạm Văn Chính", "038562333" },
                    { 4, "Cầu Giấy-Hà Nội", "phuong14@gmail.com", "Trần Văn Phương", "0962345123" },
                    { 5, "Long Biên-Hà Nội", "dung15@gmail.com", "Vũ Trung Dũng", "0966167254" },
                    { 6, "Hà Đông-Hà Nội", "tuan16@gmail.com", "Phạm Anh Tuấn", "0335189109" },
                    { 7, "Gia Lâm-Hà Nội", "thao17@gmail.com", "Bùi Hương Thảo", "038222562" },
                    { 8, "Mỹ Hào-Hưng Yên", "lan18@gmail.com", "Đào Thị Lan", "0966129459" },
                    { 9, "Mỹ Hào-Hưng Yên", "hoa19@gmail.com", "Phạm Thị Hoa", "0362115342" },
                    { 10, "Gia Lâm-Hà Nội", "myltt@gmail.com", "Lê Thị Thanh Mỹ", "0333123145" },
                    { 11, "Long Biên-Hà Nội", "hungln10@gmail.com", "Lê Ngọc Hùng", "033562345" },
                    { 12, "Mỹ Hào-Hưng Yên", "annph@gmail.com", "Nguyễn Phạm Hồng An", "0335226341" },
                    { 13, "Tiên Lữ-Hưng Yên", "lett11@gmail.com", "Trần Thị Lệ", "0367225116" },
                    { 14, "Long Biên-Hà Nội", "huong99@gmail.com", "Đào Thị Hường", "038876554" },
                    { 15, "Long Biên-Hà Nội", "thuantt@gmail.com", "Phạm Văn Thuận", "0362115114" },
                    { 16, "Gia Lâm-Hà Nội", "maiii@gmail.com", "Trần Thị Mai", "0962117110" },
                    { 17, "Gia Lâm-Hà Nội", "chuan111@gmail.com", "Phạm Minh Chuẩn", "0969665343" },
                    { 18, "Ân Thi-Hưng Yên", "hoanggnn@gmail.com", "Nguyễn Huy Hoàng", "038110990" },
                    { 19, "Ân Thi-Hưng Yên", "haoobb@gmail.com", "Bùi Kim Hào", "0966225100" },
                    { 20, "Gia Lâm-Hà Nội", "hieupmm@gmail.com", "Phạm Minh Hiếu", "0362111767" }
                });

            migrationBuilder.InsertData(
                table: "LogTypes",
                columns: new[] { "LogTypeId", "Description" },
                values: new object[,]
                {
                    { 1, "Withdraw" },
                    { 2, "Transfer" },
                    { 3, "Check balance" },
                    { 4, "Change PIN" }
                });

            migrationBuilder.InsertData(
                table: "Moneys",
                columns: new[] { "MoneyID", "MoneyValue" },
                values: new object[,]
                {
                    { 1, 50000.00m },
                    { 2, 100000.00m },
                    { 3, 200000.00m },
                    { 4, 500000.00m }
                });

            migrationBuilder.InsertData(
                table: "OverDarfts",
                columns: new[] { "ODID", "Value" },
                values: new object[,]
                {
                    { 1, 2000000m },
                    { 2, 3000000m },
                    { 3, 5000000m }
                });

            migrationBuilder.InsertData(
                table: "OverDarfts",
                columns: new[] { "ODID", "Value" },
                values: new object[,]
                {
                    { 4, 10000000m },
                    { 5, 20000000m },
                    { 6, 30000000m },
                    { 7, 50000000m },
                    { 8, 100000000m },
                    { 9, 150000000m },
                    { 10, 200000000m },
                    { 11, 250000000m },
                    { 12, 300000000m },
                    { 13, 350000000m },
                    { 14, 400000000m },
                    { 15, 450000000m },
                    { 16, 500000000m },
                    { 17, 600000000m },
                    { 18, 800000000m },
                    { 19, 1000000000m },
                    { 20, 5000000000m }
                });

            migrationBuilder.InsertData(
                table: "WithdrawLimits",
                columns: new[] { "WDID", "Value" },
                values: new object[,]
                {
                    { 1, 2000000m },
                    { 2, 3000000m },
                    { 3, 5000000m },
                    { 4, 10000000m },
                    { 5, 20000000m },
                    { 6, 30000000m },
                    { 7, 60000000m },
                    { 8, 90000000m },
                    { 9, 100000000m },
                    { 10, 200000000m },
                    { 11, 250000000m },
                    { 12, 300000000m },
                    { 13, 400000000m },
                    { 14, 600000000m },
                    { 15, 800000000m },
                    { 16, 1000000000m },
                    { 17, 1500000000m },
                    { 18, 1800000000m },
                    { 19, 2000000000m },
                    { 20, 2500000000m }
                });

            migrationBuilder.InsertData(
                table: "Accounts",
                columns: new[] { "AccountID", "AccountNo", "Balance", "CustID", "ODID", "WDID" },
                values: new object[,]
                {
                    { 1, "107162411168", 5000000m, 1, 5, 8 },
                    { 2, "107284511002", 10000000m, 1, 4, 11 },
                    { 3, "107345611961", 9000000m, 11, 5, 10 },
                    { 4, "108023011168", 30000000m, 19, 11, 10 },
                    { 5, "108147111168", 110000000m, 6, 13, 3 },
                    { 6, "109632145068", 5000000m, 7, 11, 4 },
                    { 7, "110234564788", 24000000m, 9, 9, 9 },
                    { 8, "111147821168", 15000000m, 11, 1, 8 },
                    { 9, "123001245968", 25000000m, 17, 2, 10 },
                    { 10, "124985201478", 20000000m, 7, 2, 11 },
                    { 11, "10730214589", 13000000m, 10, 5, 2 },
                    { 12, "10710254879", 5000000m, 10, 18, 2 },
                    { 13, "107630215847", 56000000m, 11, 10, 10 },
                    { 14, "108945128736", 55000000m, 4, 11, 19 },
                    { 15, "125200001467", 35000000m, 15, 19, 20 },
                    { 16, "125103669870", 20000000m, 13, 20, 18 },
                    { 17, "169330011254", 10000000m, 9, 5, 17 },
                    { 18, "130266955447", 2000000m, 12, 5, 7 },
                    { 19, "140232651115", 3000000m, 4, 5, 7 },
                    { 20, "141203369874", 30000000m, 20, 5, 9 }
                });

            migrationBuilder.InsertData(
                table: "Stocks",
                columns: new[] { "StockID", "ATMID", "MoneyID", "Quantity" },
                values: new object[,]
                {
                    { 1, 1, 1, 200 },
                    { 2, 1, 2, 200 },
                    { 3, 1, 3, 200 },
                    { 4, 1, 4, 200 },
                    { 5, 2, 1, 400 },
                    { 6, 2, 2, 200 },
                    { 7, 3, 1, 200 },
                    { 8, 3, 2, 200 },
                    { 9, 3, 3, 500 },
                    { 10, 4, 1, 400 },
                    { 11, 4, 2, 400 },
                    { 12, 4, 3, 400 },
                    { 13, 4, 4, 400 },
                    { 14, 5, 1, 400 },
                    { 15, 5, 2, 400 },
                    { 16, 6, 1, 400 },
                    { 17, 6, 2, 400 },
                    { 18, 7, 1, 400 },
                    { 19, 7, 2, 400 },
                    { 20, 8, 1, 400 },
                    { 21, 8, 2, 400 },
                    { 22, 9, 1, 400 }
                });

            migrationBuilder.InsertData(
                table: "Stocks",
                columns: new[] { "StockID", "ATMID", "MoneyID", "Quantity" },
                values: new object[] { 23, 9, 2, 400 });

            migrationBuilder.InsertData(
                table: "Stocks",
                columns: new[] { "StockID", "ATMID", "MoneyID", "Quantity" },
                values: new object[] { 24, 10, 1, 400 });

            migrationBuilder.InsertData(
                table: "Stocks",
                columns: new[] { "StockID", "ATMID", "MoneyID", "Quantity" },
                values: new object[] { 25, 10, 2, 400 });

            migrationBuilder.InsertData(
                table: "Cards",
                columns: new[] { "CardNo", "AccountID", "Attempt", "ExpiredDate", "PIN", "StartDate", "Status" },
                values: new object[,]
                {
                    { "0115755230042035", 14, 3, new DateTime(2025, 2, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "777821", new DateTime(2022, 2, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), "valid" },
                    { "0993755230042035", 6, 3, new DateTime(2025, 2, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "872625", new DateTime(2022, 2, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), "valid" },
                    { "1282325404183010", 10, 3, new DateTime(2025, 2, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "991711", new DateTime(2022, 2, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), "valid" },
                    { "1682002304184550", 17, 3, new DateTime(2025, 2, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "918161", new DateTime(2022, 2, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), "valid" },
                    { "1682200004184550", 9, 3, new DateTime(2025, 2, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "776554", new DateTime(2022, 2, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), "valid" },
                    { "1682325404184550", 13, 3, new DateTime(2025, 2, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "653000", new DateTime(2022, 2, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), "valid" },
                    { "2171100018413200", 20, 3, new DateTime(2025, 2, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "922511", new DateTime(2022, 2, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), "block" },
                    { "2360870160342161", 2, 3, new DateTime(2025, 2, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "188991", new DateTime(2022, 2, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), "valid" },
                    { "2821491318413200", 15, 3, new DateTime(2025, 2, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "064213", new DateTime(2022, 2, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), "valid" },
                    { "2836996267348266", 4, 3, new DateTime(2025, 2, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "167243", new DateTime(2022, 2, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), "invalid" },
                    { "4966575087181020", 3, 3, new DateTime(2025, 2, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "118116", new DateTime(2022, 2, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), "valid" },
                    { "5539328606652179", 5, 3, new DateTime(2025, 2, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "117667", new DateTime(2022, 2, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), "valid" },
                    { "6835760905033599", 8, 3, new DateTime(2025, 2, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "135111", new DateTime(2022, 2, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), "valid" },
                    { "7084000046868173", 16, 3, new DateTime(2025, 2, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "173624", new DateTime(2022, 2, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), "valid" },
                    { "7084054246868173", 11, 3, new DateTime(2025, 2, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "661551", new DateTime(2022, 2, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), "valid" },
                    { "7084321056868172", 18, 3, new DateTime(2025, 2, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "171651", new DateTime(2022, 2, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), "valid" },
                    { "8821491318413200", 7, 3, new DateTime(2025, 2, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "384738", new DateTime(2022, 2, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), "invalid" },
                    { "8907274830042035", 19, 3, new DateTime(2025, 2, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "637261", new DateTime(2022, 2, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), "block" },
                    { "9084147846861734", 12, 3, new DateTime(2025, 2, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "973521", new DateTime(2022, 2, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), "block" },
                    { "9696308315598901", 1, 3, new DateTime(2025, 2, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "131020", new DateTime(2022, 2, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), "valid" }
                });

            migrationBuilder.InsertData(
                table: "Logs",
                columns: new[] { "LogID", "ATMID", "Amount", "CardNo", "Details", "LogDate", "LogTypeID" },
                values: new object[,]
                {
                    { 1, 1, 1000000.00m, "9696308315598901", "Withdraw", new DateTime(2022, 1, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { 2, 5, 2000000.00m, "9696308315598901", "Withdraw", new DateTime(2022, 2, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { 3, 4, 3000000.00m, "9696308315598901", "Withdraw", new DateTime(2022, 6, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { 4, 1, 4000000.00m, "2360870160342161", "Le Ngoc Hung", new DateTime(2022, 5, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), 2 },
                    { 5, 5, 500000.00m, "2360870160342161", "Withdraw", new DateTime(2022, 1, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { 6, 1, 6000000.00m, "4966575087181020", "Withdraw", new DateTime(2022, 12, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { 7, 7, 3000000.00m, "2836996267348266", "Check balance", new DateTime(2022, 10, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), 3 },
                    { 8, 2, 500000.00m, "2836996267348266", "Withdraw", new DateTime(2022, 2, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { 9, 3, 9000000.00m, "5539328606652179", "Le Thi Thanh My", new DateTime(2022, 2, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), 2 },
                    { 10, 10, 0m, "5539328606652179", "Check balance", new DateTime(2022, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3 },
                    { 11, 3, 1000000.00m, "0993755230042035", "Withdraw", new DateTime(2022, 3, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { 12, 8, 5000000.00m, "8821491318413200", "Withdraw", new DateTime(2022, 5, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { 13, 10, 1000000.00m, "6835760905033599", "Withdraw", new DateTime(2022, 2, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { 14, 7, 2000000.00m, "1682200004184550", "Withdraw", new DateTime(2022, 5, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { 15, 7, 2000000.00m, "1282325404183010", "Withdraw", new DateTime(2022, 3, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { 16, 7, 5000000.00m, "1282325404183010", "Withdraw", new DateTime(2022, 5, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { 17, 7, 1000000.00m, "7084054246868173", "Withdraw", new DateTime(2022, 2, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { 18, 5, 2000000.00m, "9084147846861734", "Withdraw", new DateTime(2022, 3, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { 19, 4, 2000000.00m, "0115755230042035", "Withdraw", new DateTime(2022, 4, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { 20, 7, 5000000.00m, "0115755230042035", "Withdraw", new DateTime(2022, 4, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { 21, 4, 1000000.00m, "5539328606652179", "Withdraw", new DateTime(2022, 2, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { 22, 3, 1500000.00m, "5539328606652179", "Withdraw", new DateTime(2022, 2, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_CustID",
                table: "Accounts",
                column: "CustID");

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_ODID",
                table: "Accounts",
                column: "ODID");

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_WDID",
                table: "Accounts",
                column: "WDID");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Cards_AccountID",
                table: "Cards",
                column: "AccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Logs_ATMID",
                table: "Logs",
                column: "ATMID");

            migrationBuilder.CreateIndex(
                name: "IX_Logs_CardNo",
                table: "Logs",
                column: "CardNo");

            migrationBuilder.CreateIndex(
                name: "IX_Logs_LogTypeID",
                table: "Logs",
                column: "LogTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_Stocks_ATMID",
                table: "Stocks",
                column: "ATMID");

            migrationBuilder.CreateIndex(
                name: "IX_Stocks_MoneyID",
                table: "Stocks",
                column: "MoneyID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "Configs");

            migrationBuilder.DropTable(
                name: "Logs");

            migrationBuilder.DropTable(
                name: "Stocks");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Cards");

            migrationBuilder.DropTable(
                name: "LogTypes");

            migrationBuilder.DropTable(
                name: "ATMs");

            migrationBuilder.DropTable(
                name: "Moneys");

            migrationBuilder.DropTable(
                name: "Accounts");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "OverDarfts");

            migrationBuilder.DropTable(
                name: "WithdrawLimits");
        }
    }
}
