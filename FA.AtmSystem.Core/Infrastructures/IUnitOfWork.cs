﻿using FA.AtmSystem.Core.IRepositories;

namespace FA.AtmSystem.Core.Infrastructures
{
    public interface IUnitOfWork : IDisposable
    {
        AtmSystemContext AtmSystemContext { get; }
        IAccountRepository AccountRepository { get; }
        IOverDraftRepository OverDraftRepository { get; }
        IWithdrawLimitRepository WithdrawLimitRepository { get; }
        ICustomerRepository CustomerRepository { get; }
        ICardRepository CardRepository { get; }
        ILogRepository LogRepository { get; }
        ILogTypeRepository LogTypeRepository { get; }
        IStockRepository StockRepository { get; }
        IMoneyRepository MoneyRepository { get; }
        IATMRepository ATMRepository { get; }
        IConfigRepository ConfigRepository { get; }

        int SaveChange();
    }
}
