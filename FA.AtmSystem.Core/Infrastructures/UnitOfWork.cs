﻿using FA.AtmSystem.Core.IRepositories;
using FA.AtmSystem.Core.Repositories;
using Microsoft.Extensions.Options;

namespace FA.AtmSystem.Core.Infrastructures
{
    public class UnitOfWork : IUnitOfWork
    {
        private AtmSystemContext _context;
        private readonly IOptions<AppSettings> _appSettings;
        private IAccountRepository _accountRepository;
        private IOverDraftRepository _overDraftRepository;
        private IWithdrawLimitRepository _withdrawLimitRepository;
        private ICustomerRepository _customerRepository;
        private ICardRepository _cardRepository;
        private ILogRepository _logRepository;
        private ILogTypeRepository _logTypeRepository;
        private IStockRepository _stockRepository;
        private IMoneyRepository _moneyRepository;
        private IATMRepository _atmRepository;
        private IConfigRepository _configRepository;

        public UnitOfWork(AtmSystemContext context, IOptions<AppSettings> appsettings)
        {
            _context = context;
            _appSettings = appsettings;
        }
        public AtmSystemContext AtmSystemContext => _context;

        public IAccountRepository AccountRepository => _accountRepository ?? (_accountRepository = new AccountRepository(_context));

        public IOverDraftRepository OverDraftRepository => _overDraftRepository ?? (_overDraftRepository = new OverDraftRepository(_context));

        public IWithdrawLimitRepository WithdrawLimitRepository => _withdrawLimitRepository ?? (_withdrawLimitRepository = new WithdrawLimitRepository(_context));

        public ICustomerRepository CustomerRepository => _customerRepository ?? (_customerRepository = new CustomerRepository(_context));

        public ICardRepository CardRepository => _cardRepository ?? (_cardRepository = new CardRepository(_context));

        public ILogRepository LogRepository => _logRepository ?? (_logRepository = new LogRepository(_context));

        public ILogTypeRepository LogTypeRepository => _logTypeRepository ?? (_logTypeRepository = new LogTypeRepository(_context));

        public IStockRepository StockRepository => _stockRepository ?? (_stockRepository = new StockRepository(_context));

        public IMoneyRepository MoneyRepository => _moneyRepository ?? (_moneyRepository = new MoneyRepository(_context));

        public IATMRepository ATMRepository => _atmRepository ?? (_atmRepository = new ATMRepository(_context,_appSettings));

        public IConfigRepository ConfigRepository => _configRepository ?? (_configRepository = new ConfigRepository(_context));

        public void Dispose()
        {
            _context.Dispose();
        }

        public int SaveChange()
        {
            return _context.SaveChanges();
        }
    }
}
