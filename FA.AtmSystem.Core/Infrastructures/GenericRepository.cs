﻿using Microsoft.EntityFrameworkCore;

namespace FA.AtmSystem.Core.Infrastructures
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private readonly AtmSystemContext _context;
        private readonly DbSet<T> _dbSet;

        public GenericRepository(AtmSystemContext context)
        {
            _context = context;
            _dbSet = _context.Set<T>();
        }
        public void Add(T entity)
        {
            _dbSet.Add(entity);
        }

        public void Delete(T entity)
        {
            _dbSet.Remove(entity);
        }

        public void Delete(int id)
        {
            var entity = _dbSet.Find(id);
            if (entity == null)
                throw new ArgumentNullException($"Do not find {entity?.GetType().Name} with Id={id}");
            _dbSet.Remove(entity);
        }

        public T Find(int id)
        {
            var entity = _dbSet.Find(id);
            if (entity == null)
                throw new ArgumentNullException($"Do not find {entity?.GetType().Name} with Id={id}");
            return entity;
        }

        public IEnumerable<T> Find(Func<T, bool> condition)
        {
            return _dbSet.Where(condition);
        }

        public IEnumerable<T> GetAll()
        {
            return _dbSet;
        }

        public void Update(T entity)
        {
            _dbSet.Update(entity);
        }
    }
}
