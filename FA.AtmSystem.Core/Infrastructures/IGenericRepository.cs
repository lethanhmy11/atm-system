﻿namespace FA.AtmSystem.Core.Infrastructures
{
    public interface IGenericRepository<T> where T : class
    {
        T Find(int id);
        IEnumerable<T> Find(Func<T, bool> condition);
        void Add(T entity);
        void Delete(T entity);
        void Delete(int id);
        void Update(T entity);
        IEnumerable<T> GetAll();

    }

}
