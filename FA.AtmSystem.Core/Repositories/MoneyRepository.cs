﻿using FA.AtmSystem.Core.Infrastructures;
using FA.AtmSystem.Core.IRepositories;
using FA.AtmSystem.Core.Models;

namespace FA.AtmSystem.Core.Repositories
{
    public class MoneyRepository : GenericRepository<Money>, IMoneyRepository
    {
        public MoneyRepository(AtmSystemContext context) : base(context)
        {
        }
    }
}
