﻿using FA.AtmSystem.Core.Infrastructures;
using FA.AtmSystem.Core.IRepositories;
using FA.AtmSystem.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace FA.AtmSystem.Core.Repositories
{
    public class AccountRepository : GenericRepository<Account>, IAccountRepository
    {
        private readonly AtmSystemContext _context;

        public AccountRepository(AtmSystemContext context):base(context)
        {
            _context = context;
        }

        public bool CheckAccountIsValid(string AccountNo)
        {
            var account = _context.Accounts.Join(_context.Cards, a => a.AccountID, b => b.AccountID, (a, b) => new
            {
                AccountNo = a.AccountNo,
                CardNo = b.CardNo,
                StatusCard = b.Status
            }).Where(p => p.AccountNo == AccountNo && p.StatusCard.Equals("valid")).FirstOrDefault();
            if (account == null)
            {
                return false;
            }
            return true;
        }

        public decimal CheckBalance(string CardNo)
        {
            var card = _context.Cards.Include(p => p.Account).FirstOrDefault(p => p.CardNo == CardNo);
            return card.Account.Balance;
        }

        public bool CheckConfigWithdraw(decimal WithdrawAmount)
        {
            var config = _context.Configs.FirstOrDefault();
            if (WithdrawAmount < config.MinWithdraw || WithdrawAmount > config.MaxWithdraw)
                return false;
            return true;
        }

        public bool CheckEnoughMoneyToWithdraw(decimal WithdrawAmount, string CardNo)
        {
            var card = _context.Cards.Include(a => a.Account).Where(s => s.CardNo == CardNo).FirstOrDefault();
            var overDraft = _context.OverDarfts.Where(p => p.ODID == card.Account.ODID).FirstOrDefault();
            if (WithdrawAmount <= card.Account.Balance + overDraft.Value)
                return true;
            return false;
        }

        public bool CheckWithdrawCash(decimal WithdrawAmount, string CardNo)
        {
            //Tổng số tiền đã rút trong hôm nay
            decimal withdrawAmountMoney = 0;
            var logs = _context.Logs.Where(p => p.CardNo == CardNo && p.LogDate.Day == DateTime.Now.Day && p.LogTypeID == 1).ToList();
            foreach (var item in logs)
            {
                withdrawAmountMoney += item.Amount;
            }

            var card = _context.Cards.FirstOrDefault(p => p.CardNo == CardNo);
            var account = _context.Accounts.FirstOrDefault(p => p.AccountID == card.AccountID);
            //Hạn mức của Card
            var withdrawLimit = _context.WithdrawLimits.FirstOrDefault(p => p.WDID == account.WDID);
            var overDraft = _context.OverDarfts.FirstOrDefault(p => p.ODID == account.ODID);
            if (withdrawAmountMoney + WithdrawAmount <= withdrawLimit.Value + overDraft.Value)
                return true;
            return false;
        }
    }
}
