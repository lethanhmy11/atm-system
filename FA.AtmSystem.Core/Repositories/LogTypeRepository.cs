﻿using FA.AtmSystem.Core.Infrastructures;
using FA.AtmSystem.Core.IRepositories;
using FA.AtmSystem.Core.Models;

namespace FA.AtmSystem.Core.Repositories
{
    public class LogTypeRepository : GenericRepository<LogType>, ILogTypeRepository
    {
        public LogTypeRepository(AtmSystemContext context) : base(context)
        {
        }
    }
}
