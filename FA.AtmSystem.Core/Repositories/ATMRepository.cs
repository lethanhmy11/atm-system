﻿using FA.AtmSystem.Core.Infrastructures;
using FA.AtmSystem.Core.IRepositories;
using FA.AtmSystem.Core.Models;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace FA.AtmSystem.Core.Repositories
{
    public class ATMRepository : GenericRepository<ATM>, IATMRepository
    {
        private readonly AtmSystemContext _context;
        private readonly AppSettings _appSettings;

        public ATMRepository(AtmSystemContext context) : base(context)
        {
            _context = context;
        }

        public ATMRepository(AtmSystemContext context, IOptions<AppSettings> appsettings) : base(context)
        {
            _context = context;
            _appSettings = appsettings.Value;
        }

        public Card IncreaseAttempt(string cardNo)
        {
            // Get card from DB
            var card = _context.Cards.SingleOrDefault(x => x.CardNo == cardNo);
            card.Attempt++;
            _context.SaveChanges();
            return card;
        }

        public Card ResetAttempt(string cardNo)
        {
            // Get card from DB
            var card = _context.Cards.SingleOrDefault(x => x.CardNo == cardNo);
            card.Attempt = 0;
            _context.SaveChanges();
            return card;
        }

        public Card SwallowCard(string cardNo)
        {
            // Get card from DB
            var card = _context.Cards.SingleOrDefault(x => x.CardNo == cardNo);
            card.Status = "block";
            _context.SaveChanges();
            return card;
        }

        // Check if card number readable
        public Card ValidateCard(string cardNumber)
        {
            // Check if card number is positive int - correct format
            if (long.TryParse(cardNumber, out long number))
            {
                // Get card from DB
                var card = _context.Cards.SingleOrDefault(x => x.CardNo == cardNumber);

                // card not found
                if (card == null || card.Status == "block" || card.Status == "invalid")
                {
                    return null;
                }
                
                return card;
            }
            return null;

        }

        public Card ValidatePIN(string cardNo, string inputPIN)
        {
            // Get card from DB
            var card = _context.Cards.SingleOrDefault(x => x.CardNo == cardNo);

            // Validate successfully
            if (card.PIN == inputPIN)
            {
                //if card was found generate JWT Token
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_appSettings.Secret);

                //Describe token
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[] {
                        // Use the Id of User as Claim Name
                        new Claim(ClaimTypes.Name, card.CardNo),
                        new Claim(ClaimTypes.Sid, card.PIN),
                        new Claim(ClaimTypes.Role, "Customer")
                    }),
                    Expires = DateTime.UtcNow.AddMinutes(30),
                    SigningCredentials = new SigningCredentials
                                    (new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };

                var token = tokenHandler.CreateToken(tokenDescriptor);
                card.Token = tokenHandler.WriteToken(token);
                return card;
            }
            return null;
        }

        public ATM ValidateATM(int ATMId)
        {
            // Get card from DB
            var atm = _context.ATMs.SingleOrDefault(x => x.ATMID == ATMId);

            // Validate successfully


            if(atm.ATMID.ToString() != null)
            {
                //if card was found generate JWT Token
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_appSettings.Secret);

                //Describe token
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[] {
                        // Use the Id of User as Claim Name
                        new Claim(ClaimTypes.Name, atm.ATMID.ToString()),
                        new Claim(ClaimTypes.Sid, atm.Branch),
                        new Claim(ClaimTypes.Role, "ATM")

                    }),
                    Expires = DateTime.UtcNow.AddMinutes(30),
                    SigningCredentials = new SigningCredentials
                                    (new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };

                var token = tokenHandler.CreateToken(tokenDescriptor);
                atm.Token = tokenHandler.WriteToken(token);

                return atm;
            }
            else
            {
                return null;
            }
                       
        }
    }
}
