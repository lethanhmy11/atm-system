﻿using FA.AtmSystem.Core.Infrastructures;
using FA.AtmSystem.Core.IRepositories;
using FA.AtmSystem.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace FA.AtmSystem.Core.Repositories
{
    public class StockRepository : GenericRepository<Stock>, IStockRepository
    {
        private readonly AtmSystemContext _context;

        public StockRepository(AtmSystemContext context) : base(context)
        {
            _context = context;
        }

        public int[] ChangeWithDrawMoney(decimal WithdrawAmount, int ATMId)
        {
            var typeMoneys=_context.Stocks.Include(p=>p.Money).Where(p=>p.ATMID==ATMId).OrderByDescending(p=>p.Money.MoneyValue).ToList();
            //Lấy số lượng tiền mỗi loại có trong atm
            int[] availableMoney = new int[typeMoneys.Count];
            int[] moneys=new int[typeMoneys.Count];
            for(int i = 0; i < typeMoneys.Count; i++)
            {
                availableMoney[i] = _context.Stocks.Where(p => p.MoneyID == typeMoneys[i].MoneyID && p.ATMID == ATMId).FirstOrDefault().Quantity;
                moneys[i] = Convert.ToInt32(typeMoneys[i].Money.MoneyValue);
            }
            int ammount = Convert.ToInt32(WithdrawAmount);
           // int[] moneys = new int[] { 500000, 200000, 100000, 50000 };
            int[] take = new int[typeMoneys.Count];
            for (int i = 0; i < typeMoneys.Count; i++)
            {
                int count = ammount / moneys[i];
                if (count > availableMoney[i])
                {
                    count = availableMoney[i];
                    ammount = ammount - availableMoney[i] * moneys[i];
                    take[i] = count;
                }
                else
                {
                    ammount = ammount - count * moneys[i];
                    take[i] = count;
                }
            }
            return take;
        }

        public decimal GetAllMoney(int ATMId)
        {
            var result = _context.Stocks.Include(s => s.Money).Where(s => s.ATMID == ATMId).ToList();
            decimal amountMoney = 0;
            foreach (var item in result)
            {
                amountMoney += item.Money.MoneyValue * item.Quantity;
            }
            return amountMoney;
        }

        public decimal GetAllMoneyOfMoneyId(int MoneyId, int ATMId)
        {
            var result = _context.Stocks.Where(s => s.MoneyID == MoneyId && s.ATMID == ATMId).ToList();
            var money = _context.Moneys.FirstOrDefault(m => m.MoneyID == MoneyId);
            decimal amountMoney = 0;
            foreach (var item in result)
            {
                amountMoney += money.MoneyValue * item.Quantity;
            }
            return amountMoney;
        }
    }
}

