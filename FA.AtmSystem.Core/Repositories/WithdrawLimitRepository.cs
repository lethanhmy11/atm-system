﻿using FA.AtmSystem.Core.Infrastructures;
using FA.AtmSystem.Core.IRepositories;
using FA.AtmSystem.Core.Models;

namespace FA.AtmSystem.Core.Repositories
{
    public class WithdrawLimitRepository : GenericRepository<WithdrawLimit>, IWithdrawLimitRepository
    {
        public WithdrawLimitRepository(AtmSystemContext context) : base(context)
        {
        }
    }
}
