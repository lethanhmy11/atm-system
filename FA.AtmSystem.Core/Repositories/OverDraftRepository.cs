﻿using FA.AtmSystem.Core.Infrastructures;
using FA.AtmSystem.Core.IRepositories;
using FA.AtmSystem.Core.Models;

namespace FA.AtmSystem.Core.Repositories
{
    public class OverDraftRepository : GenericRepository<OverDraft>, IOverDraftRepository
    {
        public OverDraftRepository(AtmSystemContext context) : base(context)
        {
        }
    }

}
