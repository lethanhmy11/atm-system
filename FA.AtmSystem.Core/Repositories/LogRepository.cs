﻿using FA.AtmSystem.Core.Infrastructures;
using FA.AtmSystem.Core.IRepositories;
using FA.AtmSystem.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace FA.AtmSystem.Core.Repositories
{
    public class LogRepository : GenericRepository<Log>, ILogRepository
    {
        private readonly AtmSystemContext _context;

        public LogRepository(AtmSystemContext context) : base(context)
        {
            _context = context;
        }

        public ICollection<Log> GetLogsInADurationBefore(string cardNo, int duration, int index)
        {
            var date = DateTime.Now;
            var rowPerPage = 3;
            var fromDate = date.AddDays(-duration);
            return _context.Logs.Include(l => l.ATM).Include(l => l.LogType).Where(l => l.LogDate >= fromDate && l.CardNo == cardNo)
                .OrderByDescending(l => l.LogDate).Skip(index * rowPerPage).Take(rowPerPage).ToList();
        }
    }
}
