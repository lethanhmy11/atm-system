﻿using FA.AtmSystem.Core.Infrastructures;
using FA.AtmSystem.Core.IRepositories;
using FA.AtmSystem.Core.Models;

namespace FA.AtmSystem.Core.Repositories
{
    public class ConfigRepository : GenericRepository<Config>, IConfigRepository
    {
        public ConfigRepository(AtmSystemContext context) : base(context)
        {
        }
    }
}
