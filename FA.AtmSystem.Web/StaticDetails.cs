﻿namespace FA.AtmSystem.Web
{
    public static class StaticDetails
    {
        public static string APIBaseUrl = "https://localhost:7260/";
        public static string ATMAPIPath = APIBaseUrl + "api/ATMs/";
        public static string CustomerAPIPath = APIBaseUrl + "api/Customer/";
        public static string AccountAPIPath = APIBaseUrl + "api/Account/";
        public static string LogAPIPath = APIBaseUrl + "api/Log/";
        public static string StockAPIPath = APIBaseUrl + "api/Stock/";
        public static string CardAPIPath = APIBaseUrl + "api/Card/";
    }
}
