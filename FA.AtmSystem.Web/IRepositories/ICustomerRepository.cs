﻿using FA.AtmSystem.Core.Models;
using FA.AtmSystem.ViewModels.Accounts;
using FA.AtmSystem.ViewModels.LogVMs;
using FA.AtmSystem.Web.Infrastructures;

namespace FA.AtmSystem.Web.IRepositories
{
    public interface ICustomerRepository 
    {
        Task<LogViewModel>  WithDraw(string url,int ATMId, decimal WithdrawAmount, string CardNo, string token);
        Task<LogViewModel> Transfer(string url,int ATMId, decimal WithdrawAmount, string CardNo, string AcountNo, string token);
        Task<AccountViewModel> CheckBalance(string url, string CardNo, string token);
        Task<bool> ChangePinAsync(string url, string cardNo, string oldPIN, string newPIN, string rePIN, int atmID, string token);

    }
}
