﻿using FA.AtmSystem.ViewModels.Cards;
using FA.AtmSystem.Web.Infrastructures;

namespace FA.AtmSystem.Web.IRepositories
{
    public interface ICardRepository 
    {
        Task<CardVM> GetCard(string url, string cardNo, string token);
    }
}
