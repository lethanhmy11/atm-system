﻿using FA.AtmSystem.Cards.ViewModels;
using FA.AtmSystem.Core.Models;
using FA.AtmSystem.Web.Infrastructures;

namespace FA.AtmSystem.Web.IRepositories
{
    public interface IATMRepository : IGenericRepository<Card>
    {
        Task<Card> ValidateCardAsync(string url, CardValidateVM objToCreate,string token);
        Task<Card> ValidateCardPINAsync(string url, CardValidatePINVM objToCreate, string token);
        Task<Card> ResetCardAttemptAsync(string url, CardValidateVM card, string token);
        Task<Card> IncreaseCardAttemptAsync(string url, CardValidateVM card, string token);
        Task<Card> SwallowCardAsync(string url, CardValidateVM card, string token);
        Task<IEnumerable<ATM>> GetATMsAsync(string url, string token);
        Task<ATM> ValidateATMAsync(string url, int atmID);
    }
}
