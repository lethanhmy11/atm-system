﻿using FA.AtmSystem.ViewModels.Accounts;

namespace FA.AtmSystem.Web.IRepositories
{
    public interface IAccountRepository
    {
        Task<bool> CheckAccountIsValid(string url, string AccountNo, string token);
        Task<AccountViewModel> GetAccount(string url, string AccountNo, string token);
        Task<bool> EnoughMoneyToTransfer(string url, string AccountNo, decimal moneyTransfer, string token);
    }
}
