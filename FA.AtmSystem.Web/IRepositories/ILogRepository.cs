﻿using FA.AtmSystem.ViewModels.ViewHistoryDtos;
using FA.AtmSystem.Web.Infrastructures;

namespace FA.AtmSystem.Web.IRepositories
{
    public interface ILogRepository : IGenericRepository<ViewHistoryLogDto>
    {
    }
}
