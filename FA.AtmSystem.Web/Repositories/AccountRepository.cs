﻿using FA.AtmSystem.ViewModels.Accounts;
using FA.AtmSystem.Web.IRepositories;
using Newtonsoft.Json;

namespace FA.AtmSystem.Web.Repositories
{
    public class AccountRepository : IAccountRepository
    {
        private readonly IHttpClientFactory _clientFactory;

        public AccountRepository(IHttpClientFactory httpClientFactory)
        {
            _clientFactory = httpClientFactory;
        }

        public async Task<bool> CheckAccountIsValid(string url, string AccountNo, string token)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url + "CheckAccountIsValid/" + AccountNo);
            var client = _clientFactory.CreateClient();
            if (token != null && token.Length != 0)
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            }
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return true;
            }
            return false;
        }

        public async Task<bool> EnoughMoneyToTransfer(string url, string AccountNo, decimal moneyTransfer, string token)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url + "GetAccount/" + AccountNo);
            var client = _clientFactory.CreateClient();
            if (token != null && token.Length != 0)
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            }
            HttpResponseMessage respone = await client.SendAsync(request);
            var result = await respone.Content.ReadAsStringAsync();
            var accountVM= JsonConvert.DeserializeObject<AccountViewModel>(result);
            if (moneyTransfer > accountVM.Balance)
                return false;
            return true;
        }

        public async Task<AccountViewModel> GetAccount(string url, string AccountNo, string token)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url + "GetAccount/" + AccountNo);
            var client = _clientFactory.CreateClient();
            if (token != null && token.Length != 0)
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            }
            HttpResponseMessage respone = await client.SendAsync(request);
            var accountVM = await respone.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<AccountViewModel>(accountVM);
        }
    }
}
