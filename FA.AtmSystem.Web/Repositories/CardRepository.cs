﻿using FA.AtmSystem.ViewModels.Cards;
using FA.AtmSystem.Web.Infrastructures;
using FA.AtmSystem.Web.IRepositories;
using Newtonsoft.Json;

namespace FA.AtmSystem.Web.Repositories
{
    public class CardRepository :  ICardRepository
    {
        private readonly IHttpClientFactory _clientFactory;
        public CardRepository(IHttpClientFactory clientFactory) 
        {
            _clientFactory = clientFactory;
        }

        public async Task<CardVM> GetCard(string url, string cardNo, string token)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "GetCard/" + cardNo);
            var client = _clientFactory.CreateClient();
            if (token != null && token.Length != 0)
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            }
            HttpResponseMessage respone = await client.SendAsync(request);
            var accountVM = await respone.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<CardVM>(accountVM);
        }
    }
}
