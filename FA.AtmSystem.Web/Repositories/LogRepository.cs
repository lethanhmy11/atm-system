﻿using FA.AtmSystem.ViewModels.ViewHistoryDtos;
using FA.AtmSystem.Web.Infrastructures;
using FA.AtmSystem.Web.IRepositories;

namespace FA.AtmSystem.Web.Repositories
{
    public class LogRepository : GenericRepository<ViewHistoryLogDto>, ILogRepository
    {
        private readonly IHttpClientFactory _clientFactory;
        public LogRepository(IHttpClientFactory clientFactory) : base(clientFactory)
        {
            _clientFactory = clientFactory;
        }


    }
}
