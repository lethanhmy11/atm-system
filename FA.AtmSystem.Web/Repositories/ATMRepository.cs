﻿using FA.AtmSystem.Cards.ViewModels;
using FA.AtmSystem.Core.Models;
using FA.AtmSystem.Web.Infrastructures;
using FA.AtmSystem.Web.IRepositories;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Text;

namespace FA.AtmSystem.Web.Repositories
{
    public class ATMRepository : GenericRepository<Card>, IATMRepository
    {
        private readonly IHttpClientFactory _clientFactory;
        public ATMRepository(IHttpClientFactory clientFactory) : base(clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<IEnumerable<ATM>> GetATMsAsync(string url, string token)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url + "GetATMs");
            

            var client = _clientFactory.CreateClient();
            if (token != null && token.Length != 0)
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
            HttpResponseMessage response = await client.SendAsync(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<ATM>>(jsonString);
            }
            return null;
        }

        public async Task<Card> IncreaseCardAttemptAsync(string url, CardValidateVM card, string token)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url);
            if (card != null)
            {
                request.Content = new StringContent(
                    JsonConvert.SerializeObject(card), Encoding.UTF8, "application/json");
            }
            else
            {
                return new Card();
            }

            var client = _clientFactory.CreateClient();
            if (token != null && token.Length != 0)
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
            HttpResponseMessage response = await client.SendAsync(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Card>(jsonString);
            }
            else
            {
                return new Card();
            }
        }

        public async Task<Card> ResetCardAttemptAsync(string url, CardValidateVM card, string token)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url);
            if (card != null)
            {
                request.Content = new StringContent(
                    JsonConvert.SerializeObject(card), Encoding.UTF8, "application/json");
            }
            else
            {
                return new Card();
            }

            var client = _clientFactory.CreateClient();
            if (token != null && token.Length != 0)
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
            HttpResponseMessage response = await client.SendAsync(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Card>(jsonString);
            }
            else
            {
                return new Card();
            }
        }

        public async Task<Card> SwallowCardAsync(string url, CardValidateVM card, string token)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url);
            if (card != null)
            {
                request.Content = new StringContent(
                    JsonConvert.SerializeObject(card), Encoding.UTF8, "application/json");
            }
            else
            {
                return new Card();
            }

            var client = _clientFactory.CreateClient();
            if (token != null && token.Length != 0)
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
            HttpResponseMessage response = await client.SendAsync(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Card>(jsonString);
            }
            else
            {
                return new Card();
            }
        }

        public async Task<Card> ValidateCardAsync(string url, CardValidateVM objToCreate, string token)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url);
            if (objToCreate != null)
            {
                request.Content = new StringContent(
                    JsonConvert.SerializeObject(objToCreate), Encoding.UTF8, "application/json");
            }
            else
            {
                return new Card();
            }

            var client = _clientFactory.CreateClient();
            if (token != null && token.Length != 0)
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
            HttpResponseMessage response = await client.SendAsync(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Card>(jsonString);
            }
            else
            {
                return new Card();
            }
        }

        public async Task<Card> ValidateCardPINAsync(string url, CardValidatePINVM objToCreate, string token)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url);
            if (objToCreate != null)
            {
                request.Content = new StringContent(
                    JsonConvert.SerializeObject(objToCreate), Encoding.UTF8, "application/json");
            }
            else
            {
                return new Card();
            }

            var client = _clientFactory.CreateClient();
            if (token != null && token.Length != 0)
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
            HttpResponseMessage response = await client.SendAsync(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Card>(jsonString);
            }
            else
            {
                return new Card();
            }
        }



        public async Task<ATM> ValidateATMAsync(string url, int atmID)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url + "ValidateATM/" + atmID);
           

            var client = _clientFactory.CreateClient();
            HttpResponseMessage response = await client.SendAsync(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<ATM>(jsonString);
            }
            else
            {
                return null;
            }
        }
    }
}
