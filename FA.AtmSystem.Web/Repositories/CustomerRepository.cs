﻿using FA.AtmSystem.Core.Models;
using FA.AtmSystem.ViewModels.Accounts;
using FA.AtmSystem.ViewModels.LogVMs;
using FA.AtmSystem.Web.Infrastructures;
using FA.AtmSystem.Web.IRepositories;
using Newtonsoft.Json;
using System.Net.Http.Headers;

namespace FA.AtmSystem.Web.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly IHttpClientFactory _clientFactory;

        public CustomerRepository(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<bool> ChangePinAsync(string url, string cardNo, string oldPIN, string newPIN, string rePIN, int atmID,string token)
        {
            var request = new HttpRequestMessage(HttpMethod.Patch, url + "ChangePIN" + "/" + cardNo + "/" + oldPIN + "/" + newPIN + "/" + rePIN + "/" + atmID);


            var client = _clientFactory.CreateClient();
            if (token != null && token.Length != 0)
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
            HttpResponseMessage response = await client.SendAsync(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<AccountViewModel> CheckBalance(string url, string CardNo, string token)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url + "CheckBalance/" +CardNo);
            var client = _clientFactory.CreateClient();
            if (token != null && token.Length != 0)
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
            HttpResponseMessage respone = await client.SendAsync(request);
            var accountVM = await respone.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<AccountViewModel>(accountVM);
            
        }

        public async Task<LogViewModel> Transfer(string url, int ATMId, decimal WithdrawAmount, string CardNo, string AcountNo, string token)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url + "Transfer/" + ATMId + "/" + WithdrawAmount + "/" + CardNo+"/"+AcountNo);
            var client = _clientFactory.CreateClient();
            if (token != null && token.Length != 0)
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
            HttpResponseMessage respone = await client.SendAsync(request);
            if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var jsonString = await respone.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<LogViewModel>(jsonString);
            }
            var logVM = await respone.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<LogViewModel>(logVM);
        }

        public async Task<LogViewModel> WithDraw(string url, int ATMId, decimal WithdrawAmount, string CardNo, string token)
        {
            //if (WithdrawAmount % 50000 == 0)
            //{ 
            var request = new HttpRequestMessage(HttpMethod.Post, url + "WithDraw/" + ATMId + "/" + WithdrawAmount + "/" + CardNo);
            var client = _clientFactory.CreateClient();
            if (token != null && token.Length != 0)
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            }
            HttpResponseMessage respone =await client.SendAsync(request);
            //if (respone.StatusCode == System.Net.HttpStatusCode.OK)
            //{
            //    var jsonString = await respone.Content.ReadAsStringAsync();
            //    return JsonConvert.DeserializeObject<LogViewModel>(jsonString);
            //}
            var logVM = await respone.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<LogViewModel>(logVM);

        }
    }
}
