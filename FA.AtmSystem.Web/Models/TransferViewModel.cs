﻿namespace FA.AtmSystem.Web.Models
{
    public class TransferViewModel
    {
        public string AccountNo { get; set; }
        public string Name { get; set; }
        public decimal MoneyTransfer { get; set; }
    }
}
