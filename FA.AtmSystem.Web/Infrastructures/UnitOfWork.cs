﻿using FA.AtmSystem.Web.IRepositories;
using FA.AtmSystem.Web.Repositories;

namespace FA.AtmSystem.Web.Infrastructures
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IHttpClientFactory _clientFactory;
        private IATMRepository _atmRepository;
        private IAccountRepository _accountRepository;
        private ICustomerRepository _customerRepository;
        private ICardRepository _cardRepository;
        private ILogRepository _logRepository;

        public UnitOfWork(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public IATMRepository ATMRepository => _atmRepository ?? (_atmRepository = new ATMRepository(_clientFactory));

        public IAccountRepository AccountRepository => _accountRepository ?? (_accountRepository = new AccountRepository(_clientFactory));

        public ICustomerRepository CustomerRepository => _customerRepository ?? (_customerRepository = new CustomerRepository(_clientFactory));
        public ICardRepository CardRepository => _cardRepository ?? (_cardRepository = new CardRepository(_clientFactory));
        public ILogRepository LogRepository => _logRepository ?? (_logRepository = new LogRepository(_clientFactory));
    }
}
