﻿using FA.AtmSystem.Web.IRepositories;

namespace FA.AtmSystem.Web.Infrastructures
{
    public interface IUnitOfWork
    {
        IATMRepository ATMRepository { get; }
        IAccountRepository AccountRepository { get; }
        ICustomerRepository CustomerRepository { get; }
        ICardRepository CardRepository { get; }
        ILogRepository LogRepository { get; }
    }
}
