﻿using FA.AtmSystem.Web.Infrastructures;
using Microsoft.AspNetCore.Mvc;

namespace FA.AtmSystem.Web.Controllers
{
    public class LogViewHistoryController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        public LogViewHistoryController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetViewHistoryInADuration(int duration, int index = 0)
        {
            ViewBag.Duration = duration;
            ViewBag.Page = index;
            string cardNo = HttpContext.Session.GetString("CardNo");
            var result = await _unitOfWork.LogRepository.GetAllAsync(StaticDetails.LogAPIPath + "GetAllOfHistoryTransactions/" + cardNo + "/" + duration + "/" + index);

            return View(result);

        }
    }
}
