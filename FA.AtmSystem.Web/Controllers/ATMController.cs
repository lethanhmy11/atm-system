﻿
using FA.AtmSystem.Cards.ViewModels;
using FA.AtmSystem.Core.Models;
using FA.AtmSystem.Web.Infrastructures;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace FA.AtmSystem.Web.Controllers
{
    public class ATMController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        public ATMController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        [Authorize]
        public IActionResult Index()
        {
            TempData["CardNo"] = HttpContext.Session.GetString("CardNo");
            
            return View();
        }

        [HttpGet]
        public IActionResult EjectCard()
        {
            return View();
        }

        [HttpGet]
        public IActionResult SwallowCard()
        {
            return View();
        }

        [HttpPost]
        public IActionResult ValidateWait()
        {
            return View();
        }

        [HttpGet]
        public IActionResult ValidateCard()
        {
            CardValidateVM obj = new CardValidateVM();
            return View(obj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ValidateCard(CardValidateVM obj)
        {
            Card objCard = await _unitOfWork.ATMRepository.ValidateCardAsync(StaticDetails.ATMAPIPath + "ValidateCard/", obj, HttpContext.Session.GetString("JWTokenATM"));
            
            if (objCard.CardNo == null)
            {
                return RedirectToAction("EjectCard");
            }

            // Reset Card Attempt
            await _unitOfWork.ATMRepository.ResetCardAttemptAsync(StaticDetails.ATMAPIPath + "ResetCardAttempt/", obj, HttpContext.Session.GetString("JWTokenATM"));

            TempData["CardNo"] = objCard.CardNo;
            TempData["Attempt"] = "0";
            return RedirectToAction("ValidateCardPIN");
        }

        [HttpGet]
        public IActionResult ValidateCardPIN()
        {
            CardValidatePINVM obj = new CardValidatePINVM();
            
            return View(obj);
        }

        public IActionResult Goodbye()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ValidateCardPIN(CardValidatePINVM obj)
        {
            // Value of objCard is empty if valid PIN failed
            Card objCard = await _unitOfWork.ATMRepository.ValidateCardPINAsync(StaticDetails.ATMAPIPath + "ValidatePIN/", obj, HttpContext.Session.GetString("JWTokenATM"));
            CardValidateVM cardAPIInput = new CardValidateVM();
            cardAPIInput.CardNo = obj.CardNo;

            // increaseCard is always a card from DB
            Card increaseCard =  await _unitOfWork.ATMRepository.IncreaseCardAttemptAsync(StaticDetails.ATMAPIPath + "IncreaseCardAttempt/", cardAPIInput, HttpContext.Session.GetString("JWTokenATM"));

            TempData["CardNo"] = obj.CardNo;
            TempData["Attempt"] = "0";
            if (increaseCard.Attempt > 0) 
            {
                TempData["Attempt"] = "1";
            }
            if (objCard.Token == null)
            {
                // objCard.Attempt++;
                if (increaseCard.Attempt > 2)
                {
                    await _unitOfWork.ATMRepository.SwallowCardAsync(StaticDetails.ATMAPIPath + "SwallowCard/", cardAPIInput, HttpContext.Session.GetString("JWTokenATM"));
                    return RedirectToAction("SwallowCard");
                }
                return RedirectToAction("ValidateCardPIN");
            }

            // Reset Card Attempt
            await _unitOfWork.ATMRepository.ResetCardAttemptAsync(StaticDetails.ATMAPIPath + "ResetCardAttempt/", cardAPIInput, HttpContext.Session.GetString("JWTokenATM"));

            // Claim identity
            var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme);
            identity.AddClaim(new Claim(ClaimTypes.Name, objCard.CardNo));
            identity.AddClaim(new Claim(ClaimTypes.Sid, objCard.PIN));
            var principal = new ClaimsPrincipal(identity);
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);

            // Store Token and Card No in Session
            HttpContext.Session.SetString("JWToken", objCard.Token);
            HttpContext.Session.SetString("CardNo", objCard.CardNo);
            HttpContext.Session.SetString("PIN", objCard.PIN);

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            HttpContext.Session.SetString("JWToken", "");
            return RedirectToAction("Index", "Home");
        }


    }
}
