﻿using FA.AtmSystem.ViewModels.ATMs;
using FA.AtmSystem.Web.Infrastructures;
using FA.AtmSystem.Web.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace FA.AtmSystem.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        
        private readonly ILogger<HomeController> _logger;
        public HomeController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        

        

        public IActionResult Index()
        {
            
            return View();
        }

        public async Task<IActionResult> InsertCard(int ATMId, string ATMBranch, string Image)
        {
            var atm = await _unitOfWork.ATMRepository.ValidateATMAsync(StaticDetails.ATMAPIPath, ATMId);
           
            // Claim identity
            var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme);
            identity.AddClaim(new Claim(ClaimTypes.Name, ATMBranch));
            identity.AddClaim(new Claim(ClaimTypes.Sid, Image));
            var principal = new ClaimsPrincipal(identity);
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);

            // Store Token and ATM No in Session
            HttpContext.Session.SetString("JWTokenATM", atm.Token);
            HttpContext.Session.SetString("ATMId", atm.ATMID.ToString());
            HttpContext.Session.SetString("ATMBranch", atm.Branch);
            HttpContext.Session.SetString("Image", atm.Image);
            return View();
        }



        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}