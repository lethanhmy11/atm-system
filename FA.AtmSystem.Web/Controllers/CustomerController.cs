﻿using FA.AtmSystem.Web.Infrastructures;
using FA.AtmSystem.Web.IRepositories;
using FA.AtmSystem.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FA.AtmSystem.Web.Controllers
{
    public class CustomerController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public CustomerController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        public async Task<IActionResult> Index()
        {
            return View();
        }

        public async Task<IActionResult> EnterInput()
        {
            return View();
        }

        public async Task<IActionResult> WithDrawMoney(string withdrawAmmount)
        {
            if (String.IsNullOrEmpty(withdrawAmmount))
            {
                TempData["ErrorMessages"] = "Enter amount money you want to withdraw. Number you enter must be divisible to 50.0000.";
                return RedirectToAction(nameof(EnterInput));
            }
            while (withdrawAmmount.IndexOf(".") != -1)
            {
                withdrawAmmount = withdrawAmmount.Replace(".", "");
            }
            decimal WithDrawMoney = decimal.Parse(withdrawAmmount);
            
            string cardNo = HttpContext.Session.GetString("CardNo");
            int ATMId = int.Parse(HttpContext.Session.GetString("ATMId"));
            var result=await _unitOfWork.CustomerRepository.WithDraw(StaticDetails.CustomerAPIPath, ATMId, WithDrawMoney, cardNo, HttpContext.Session.GetString("JWToken"));
            if (result.Description==null)
            {
               
                return View();
            }
            TempData["ErrorMessages"] =result.Description.ToString();
            return RedirectToAction(nameof(EnterInput));
        }
        public async Task<IActionResult> RuleTransfer()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> AccountInput()
        {
            string cardNo = HttpContext.Session.GetString("CardNo");
            var result = await _unitOfWork.CardRepository.GetCard(StaticDetails.CardAPIPath, cardNo, HttpContext.Session.GetString("JWToken"));
            return View(result);
        }

        [HttpPost]
        public async Task<IActionResult> AccountInput(string AcountNoRecive)
        {

            var result=await _unitOfWork.AccountRepository.CheckAccountIsValid(StaticDetails.AccountAPIPath, AcountNoRecive, HttpContext.Session.GetString("JWToken"));
            if (result)
            {
                var account = await _unitOfWork.AccountRepository.GetAccount(StaticDetails.AccountAPIPath, AcountNoRecive, HttpContext.Session.GetString("JWToken"));
                return View("AcceptedAccountRecive", account);
            }
            return View("ReAccountInput");
        }
        
        public async Task<IActionResult> AmountInput(string name, string accountNo)
        {
            TransferViewModel obj = new TransferViewModel { Name = name, AccountNo = accountNo };         
            return View(obj);
        }
        
        
        public async Task<IActionResult> AcceptedTransfer(TransferViewModel transfer, string TransferAmount)
        {
            string cardNo = HttpContext.Session.GetString("CardNo");
            var resultCard = await _unitOfWork.CardRepository.GetCard(StaticDetails.CardAPIPath, cardNo, HttpContext.Session.GetString("JWToken"));

            if (String.IsNullOrEmpty(TransferAmount))
            {
                TempData["ErrorMessages"] = "Enter amount money you want to withdraw. Number you enter must be divisible to 50.0000.";
                return RedirectToAction(nameof(EnterInput));
            }
            while (TransferAmount.IndexOf(".") != -1)
            {
                TransferAmount = TransferAmount.Replace(".", "");
            }
            decimal TransferMoney = decimal.Parse(TransferAmount);
            var result = await _unitOfWork.AccountRepository.EnoughMoneyToTransfer(StaticDetails.AccountAPIPath, resultCard.AccountNo, TransferMoney, HttpContext.Session.GetString("JWToken"));
            if (result)
            {
                transfer.MoneyTransfer = TransferMoney;
                return View("AcceptedTransfer", transfer );
            }
            return View("ReAmountInput", transfer);
        }


        public async Task<IActionResult> Transfer(string name, string accountNo, decimal amount)
        {
            string cardNo = HttpContext.Session.GetString("CardNo");
            int ATMId = int.Parse(HttpContext.Session.GetString("ATMId"));
            var result = await _unitOfWork.CustomerRepository.Transfer(StaticDetails.CustomerAPIPath, ATMId, amount, cardNo, accountNo, HttpContext.Session.GetString("JWToken"));
            return View();
        }

        public async Task<IActionResult> Balance(string accountNo)
        {
            string cardNo = HttpContext.Session.GetString("CardNo");
            var result = await _unitOfWork.CustomerRepository.CheckBalance(StaticDetails.AccountAPIPath, cardNo, HttpContext.Session.GetString("JWToken"));
            return View(result);
        }
        public IActionResult NewPIN()
        {

            return View();
            
        }


        public IActionResult RePIN(string newPIN)
        {

            string oldPIN = HttpContext.Session.GetString("PIN");
            if (TempData.ContainsKey("newPIN"))
            {
                newPIN = TempData["newPIN"].ToString();
            }
            if (String.IsNullOrEmpty(newPIN))
            {
                TempData["ErrorMessages"] = "Enter a NEW PIN";
                return RedirectToAction(nameof(NewPIN));
            }
            if (newPIN.Length != 6)
            {
                TempData["ErrorMessages"] = "NEW PIN is 6 number";
                return RedirectToAction(nameof(NewPIN));
            }
            if (oldPIN != newPIN)
            {
                TempData["newPIN"] = newPIN;
                return View("RePIN");

            }
            else
            {
                TempData["ErrorMessages"] = "OLD PIN and NEW PIN can't coincide";
                return RedirectToAction(nameof(NewPIN));
            }

        }


        [HttpPost]
        public async Task<IActionResult> ChangePINSuccess(string rePIN)
        {
            string cardNo = HttpContext.Session.GetString("CardNo");
            int atmID = int.Parse(HttpContext.Session.GetString("ATMId"));
            string oldPIN = HttpContext.Session.GetString("PIN");
            string newPIN;
            
            if (TempData.ContainsKey("newPIN"))
            {
                newPIN = TempData["newPIN"].ToString();
                
                if (newPIN != rePIN)
                {
                    TempData["newPIN"] = newPIN;
                    TempData["ErrorMessages"] = "NEW PIN and RE-PIN don't match";
                    return RedirectToAction(nameof(RePIN));
                }
                if (String.IsNullOrEmpty(rePIN))
                {
                    TempData["newPIN"] = newPIN;
                    TempData["ErrorMessages"] = "Enter a RE-PIN";
                    return RedirectToAction(nameof(RePIN));
                }

                var status = await _unitOfWork.CustomerRepository.ChangePinAsync(StaticDetails.CustomerAPIPath, cardNo, oldPIN, newPIN, rePIN, atmID, HttpContext.Session.GetString("JWToken"));
                if (status)
                {
                    return View("ChangePINSuccess");
                }
                else
                {
                    TempData["newPIN"] = newPIN;
                    TempData["ErrorMessages"] = "Your PIN not changed";
                    return RedirectToAction(nameof(RePIN));
                }
            }
            return View("ChangePINSuccess");
            
        }
    }
}
