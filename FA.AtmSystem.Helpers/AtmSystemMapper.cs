﻿using AutoMapper;
using FA.AtmSystem.Core.Models;
using FA.AtmSystem.ViewModels.Accounts;
using FA.AtmSystem.ViewModels.Cards;
using FA.AtmSystem.ViewModels.LogVMs;
using FA.AtmSystem.ViewModels.ViewHistoryDtos;

namespace FA.AtmSystem.Helpers
{
    public class AtmSystemMapper : Profile
    {
        public AtmSystemMapper()
        {
            CreateMap<Log, LogViewModel>().ReverseMap();
            CreateMap<Log, LogCreateViewModel>().ReverseMap();
            CreateMap<Account, AccountViewModel>().ReverseMap();
            CreateMap<Card, CardVM>().ReverseMap();
            //Serve for UC4: View History
            CreateMap<Log, ViewHistoryLogDto>().ReverseMap();
            CreateMap<LogType, ViewHistoryLogTypeDto>().ReverseMap();
            CreateMap<ATM, ViewHistoryATMDto>().ReverseMap();
        }

    }
}
