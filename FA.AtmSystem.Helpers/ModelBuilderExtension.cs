﻿using FA.AtmSystem.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace FA.AtmSystem.Core
{
    public static class ModelBuilderExtension
    {
        public static void SeedData(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>().HasData(
                     new Customer() { CustID = 1, Name = "Trần Phương Nam", Phone = "0362189562", Email = "nam11@gmail.com", Addr = "Tiên Lữ-Hưng Yên" },
                     new Customer() { CustID = 2, Name = "Nguyễn Thị Yến", Phone = "033562345", Email = "yenn12@gmail.com", Addr = "Long Biên-Hà Nội" },
                     new Customer() { CustID = 3, Name = "Phạm Văn Chính", Phone = "038562333", Email = "chinh13@gmail.com", Addr = "Long Biên-Hà Nội" },
                     new Customer() { CustID = 4, Name = "Trần Văn Phương", Phone = "0962345123", Email = "phuong14@gmail.com", Addr = "Cầu Giấy-Hà Nội" },
                     new Customer() { CustID = 5, Name = "Vũ Trung Dũng", Phone = "0966167254", Email = "dung15@gmail.com", Addr = "Long Biên-Hà Nội" },
                     new Customer() { CustID = 6, Name = "Phạm Anh Tuấn", Phone = "0335189109", Email = "tuan16@gmail.com", Addr = "Hà Đông-Hà Nội" },
                     new Customer() { CustID = 7, Name = "Bùi Hương Thảo", Phone = "038222562", Email = "thao17@gmail.com", Addr = "Gia Lâm-Hà Nội" },
                     new Customer() { CustID = 8, Name = "Đào Thị Lan", Phone = "0966129459", Email = "lan18@gmail.com", Addr = "Mỹ Hào-Hưng Yên" },
                     new Customer() { CustID = 9, Name = "Phạm Thị Hoa", Phone = "0362115342", Email = "hoa19@gmail.com", Addr = "Mỹ Hào-Hưng Yên" },
                     new Customer() { CustID = 10, Name = "Lê Thị Thanh Mỹ", Phone = "0333123145", Email = "myltt@gmail.com", Addr = "Gia Lâm-Hà Nội" },
                     new Customer() { CustID = 11, Name = "Lê Ngọc Hùng", Phone = "033562345", Email = "hungln10@gmail.com", Addr = "Long Biên-Hà Nội" },
                     new Customer() { CustID = 12, Name = "Nguyễn Phạm Hồng An", Phone = "0335226341", Email = "annph@gmail.com", Addr = "Mỹ Hào-Hưng Yên" },
                     new Customer() { CustID = 13, Name = "Trần Thị Lệ", Phone = "0367225116", Email = "lett11@gmail.com", Addr = "Tiên Lữ-Hưng Yên" },
                     new Customer() { CustID = 14, Name = "Đào Thị Hường", Phone = "038876554", Email = "huong99@gmail.com", Addr = "Long Biên-Hà Nội" },
                     new Customer() { CustID = 15, Name = "Phạm Văn Thuận", Phone = "0362115114", Email = "thuantt@gmail.com", Addr = "Long Biên-Hà Nội" },
                     new Customer() { CustID = 16, Name = "Trần Thị Mai", Phone = "0962117110", Email = "maiii@gmail.com", Addr = "Gia Lâm-Hà Nội" },
                     new Customer() { CustID = 17, Name = "Phạm Minh Chuẩn", Phone = "0969665343", Email = "chuan111@gmail.com", Addr = "Gia Lâm-Hà Nội" },
                     new Customer() { CustID = 18, Name = "Nguyễn Huy Hoàng", Phone = "038110990", Email = "hoanggnn@gmail.com", Addr = "Ân Thi-Hưng Yên" },
                     new Customer() { CustID = 19, Name = "Bùi Kim Hào", Phone = "0966225100", Email = "haoobb@gmail.com", Addr = "Ân Thi-Hưng Yên" },
                     new Customer() { CustID = 20, Name = "Phạm Minh Hiếu", Phone = "0362111767", Email = "hieupmm@gmail.com", Addr = "Gia Lâm-Hà Nội" }
            );

            modelBuilder.Entity<OverDraft>().HasData(
                     new OverDraft() { ODID = 1, Value = 2000000 },
                     new OverDraft() { ODID = 2, Value = 3000000 },
                     new OverDraft() { ODID = 3, Value = 5000000 },
                     new OverDraft() { ODID = 4, Value = 10000000 },
                     new OverDraft() { ODID = 5, Value = 20000000 },
                     new OverDraft() { ODID = 6, Value = 30000000 },
                     new OverDraft() { ODID = 7, Value = 50000000 },
                     new OverDraft() { ODID = 8, Value = 100000000 },
                     new OverDraft() { ODID = 9, Value = 150000000 },
                     new OverDraft() { ODID = 10, Value = 200000000 },
                     new OverDraft() { ODID = 11, Value = 250000000 },
                     new OverDraft() { ODID = 12, Value = 300000000 },
                     new OverDraft() { ODID = 13, Value = 350000000 },
                     new OverDraft() { ODID = 14, Value = 400000000 },
                     new OverDraft() { ODID = 15, Value = 450000000 },
                     new OverDraft() { ODID = 16, Value = 500000000 },
                     new OverDraft() { ODID = 17, Value = 600000000 },
                     new OverDraft() { ODID = 18, Value = 800000000 },
                     new OverDraft() { ODID = 19, Value = 1000000000 },
                     new OverDraft() { ODID = 20, Value = 5000000000 }
            );

            modelBuilder.Entity<WithdrawLimit>().HasData(
                      new WithdrawLimit() { WDID = 1, Value = 2000000 },
                      new WithdrawLimit() { WDID = 2, Value = 3000000 },
                      new WithdrawLimit() { WDID = 3, Value = 5000000 },
                      new WithdrawLimit() { WDID = 4, Value = 10000000 },
                      new WithdrawLimit() { WDID = 5, Value = 20000000 },
                      new WithdrawLimit() { WDID = 6, Value = 30000000 },
                      new WithdrawLimit() { WDID = 7, Value = 60000000 },
                      new WithdrawLimit() { WDID = 8, Value = 90000000 },
                      new WithdrawLimit() { WDID = 9, Value = 100000000 },
                      new WithdrawLimit() { WDID = 10, Value = 200000000 },
                      new WithdrawLimit() { WDID = 11, Value = 250000000 },
                      new WithdrawLimit() { WDID = 12, Value = 300000000 },
                      new WithdrawLimit() { WDID = 13, Value = 400000000 },
                      new WithdrawLimit() { WDID = 14, Value = 600000000 },
                      new WithdrawLimit() { WDID = 15, Value = 800000000 },
                      new WithdrawLimit() { WDID = 16, Value = 1000000000 },
                      new WithdrawLimit() { WDID = 17, Value = 1500000000 },
                      new WithdrawLimit() { WDID = 18, Value = 1800000000 },
                      new WithdrawLimit() { WDID = 19, Value = 2000000000 },
                      new WithdrawLimit() { WDID = 20, Value = 2500000000 }
            );

            modelBuilder.Entity<Account>().HasData(
                     new Account() { AccountID = 1, CustID = 1, AccountNo = "107162411168", ODID = 5, WDID = 8, Balance = 5000000 },
                     new Account() { AccountID = 2, CustID = 1, AccountNo = "107284511002", ODID = 4, WDID = 11, Balance = 10000000 },
                     new Account() { AccountID = 3, CustID = 11, AccountNo = "107345611961", ODID = 5, WDID = 10, Balance = 9000000 },
                     new Account() { AccountID = 4, CustID = 19, AccountNo = "108023011168", ODID = 11, WDID = 10, Balance = 30000000 },
                     new Account() { AccountID = 5, CustID = 6, AccountNo = "108147111168", ODID = 13, WDID = 3, Balance = 110000000 },
                     new Account() { AccountID = 6, CustID = 7, AccountNo = "109632145068", ODID = 11, WDID = 4, Balance = 5000000 },
                     new Account() { AccountID = 7, CustID = 9, AccountNo = "110234564788", ODID = 9, WDID = 9, Balance = 24000000 },
                     new Account() { AccountID = 8, CustID = 11, AccountNo = "111147821168", ODID = 1, WDID = 8, Balance = 15000000 },
                     new Account() { AccountID = 9, CustID = 17, AccountNo = "123001245968", ODID = 2, WDID = 10, Balance = 25000000 },
                     new Account() { AccountID = 10, CustID = 7, AccountNo = "124985201478", ODID = 2, WDID = 11, Balance = 20000000 },
                     new Account() { AccountID = 11, CustID = 10, AccountNo = "10730214589", ODID = 5, WDID = 2, Balance = 13000000 },
                     new Account() { AccountID = 12, CustID = 10, AccountNo = "10710254879", ODID = 18, WDID = 2, Balance = 5000000 },
                     new Account() { AccountID = 13, CustID = 11, AccountNo = "107630215847", ODID = 10, WDID = 10, Balance = 56000000 },
                     new Account() { AccountID = 14, CustID = 4, AccountNo = "108945128736", ODID = 11, WDID = 19, Balance = 55000000 },
                     new Account() { AccountID = 15, CustID = 15, AccountNo = "125200001467", ODID = 19, WDID = 20, Balance = 35000000 },
                     new Account() { AccountID = 16, CustID = 13, AccountNo = "125103669870", ODID = 20, WDID = 18, Balance = 20000000 },
                     new Account() { AccountID = 17, CustID = 9, AccountNo = "169330011254", ODID = 5, WDID = 17, Balance = 10000000 },
                     new Account() { AccountID = 18, CustID = 12, AccountNo = "130266955447", ODID = 5, WDID = 7, Balance = 2000000 },
                     new Account() { AccountID = 19, CustID = 4, AccountNo = "140232651115", ODID = 5, WDID = 7, Balance = 3000000 },
                     new Account() { AccountID = 20, CustID = 20, AccountNo = "141203369874", ODID = 5, WDID = 9, Balance = 30000000 }
           );

            modelBuilder.Entity<Card>().HasData(
                     new Card() { CardNo = "9696308315598901", Status = "valid", AccountID = 1, PIN = "131020", StartDate = new DateTime(2022, 2, 14), ExpiredDate = new DateTime(2025, 2, 28), Attempt = 3 },
                     new Card() { CardNo = "2360870160342161", Status = "valid", AccountID = 2, PIN = "188991", StartDate = new DateTime(2022, 2, 14), ExpiredDate = new DateTime(2025, 2, 28), Attempt = 3 },
                     new Card() { CardNo = "4966575087181020", Status = "valid", AccountID = 3, PIN = "118116", StartDate = new DateTime(2022, 2, 14), ExpiredDate = new DateTime(2025, 2, 28), Attempt = 3 },
                     new Card() { CardNo = "2836996267348266", Status = "invalid", AccountID = 4, PIN = "167243", StartDate = new DateTime(2022, 2, 14), ExpiredDate = new DateTime(2025, 2, 28), Attempt = 3 },
                     new Card() { CardNo = "5539328606652179", Status = "valid", AccountID = 5, PIN = "117667", StartDate = new DateTime(2022, 2, 14), ExpiredDate = new DateTime(2025, 2, 28), Attempt = 3 },
                     new Card() { CardNo = "0993755230042035", Status = "valid", AccountID = 6, PIN = "872625", StartDate = new DateTime(2022, 2, 14), ExpiredDate = new DateTime(2025, 2, 28), Attempt = 3 },
                     new Card() { CardNo = "8821491318413200", Status = "invalid", AccountID = 7, PIN = "384738", StartDate = new DateTime(2022, 2, 14), ExpiredDate = new DateTime(2025, 2, 28), Attempt = 3 },
                     new Card() { CardNo = "6835760905033599", Status = "valid", AccountID = 8, PIN = "135111", StartDate = new DateTime(2022, 2, 14), ExpiredDate = new DateTime(2025, 2, 28), Attempt = 3 },
                     new Card() { CardNo = "1682200004184550", Status = "valid", AccountID = 9, PIN = "776554", StartDate = new DateTime(2022, 2, 14), ExpiredDate = new DateTime(2025, 2, 28), Attempt = 3 },
                     new Card() { CardNo = "1282325404183010", Status = "valid", AccountID = 10, PIN = "991711", StartDate = new DateTime(2022, 2, 14), ExpiredDate = new DateTime(2025, 2, 28), Attempt = 3 },
                     new Card() { CardNo = "7084054246868173", Status = "valid", AccountID = 11, PIN = "661551", StartDate = new DateTime(2022, 2, 14), ExpiredDate = new DateTime(2025, 2, 28), Attempt = 3 },
                     new Card() { CardNo = "9084147846861734", Status = "block", AccountID = 12, PIN = "973521", StartDate = new DateTime(2022, 2, 14), ExpiredDate = new DateTime(2025, 2, 28), Attempt = 3 },
                     new Card() { CardNo = "1682325404184550", Status = "valid", AccountID = 13, PIN = "653000", StartDate = new DateTime(2022, 2, 14), ExpiredDate = new DateTime(2025, 2, 28), Attempt = 3 },
                     new Card() { CardNo = "0115755230042035", Status = "valid", AccountID = 14, PIN = "777821", StartDate = new DateTime(2022, 2, 14), ExpiredDate = new DateTime(2025, 2, 28), Attempt = 3 },
                     new Card() { CardNo = "2821491318413200", Status = "valid", AccountID = 15, PIN = "064213", StartDate = new DateTime(2022, 2, 14), ExpiredDate = new DateTime(2025, 2, 28), Attempt = 3 },
                     new Card() { CardNo = "7084000046868173", Status = "valid", AccountID = 16, PIN = "173624", StartDate = new DateTime(2022, 2, 14), ExpiredDate = new DateTime(2025, 2, 28), Attempt = 3 },
                     new Card() { CardNo = "1682002304184550", Status = "valid", AccountID = 17, PIN = "918161", StartDate = new DateTime(2022, 2, 14), ExpiredDate = new DateTime(2025, 2, 28), Attempt = 3 },
                     new Card() { CardNo = "7084321056868172", Status = "valid", AccountID = 18, PIN = "171651", StartDate = new DateTime(2022, 2, 14), ExpiredDate = new DateTime(2025, 2, 28), Attempt = 3 },
                     new Card() { CardNo = "8907274830042035", Status = "block", AccountID = 19, PIN = "637261", StartDate = new DateTime(2022, 2, 14), ExpiredDate = new DateTime(2025, 2, 28), Attempt = 3 },
                     new Card() { CardNo = "2171100018413200", Status = "block", AccountID = 20, PIN = "922511", StartDate = new DateTime(2022, 2, 14), ExpiredDate = new DateTime(2025, 2, 28), Attempt = 3 }
           );

            modelBuilder.Entity<Money>().HasData(
                     new Money() { MoneyID = 1, MoneyValue = 50000.00m },
                     new Money() { MoneyID = 2, MoneyValue = 100000.00m },
                     new Money() { MoneyID = 3, MoneyValue = 200000.00m },
                     new Money() { MoneyID = 4, MoneyValue = 500000.00m }
          );

            modelBuilder.Entity<ATM>().HasData(
                    new ATM() { ATMID = 1, Branch = "Vietcomnbank", Address = "129 Đ. Lê Duẩn, Cửa Nam, Hoàn Kiếm, Hà Nội", Image = "Vietcombank.webp" },
                    new ATM() { ATMID = 2, Branch = "Techcomnbank", Address = "461, Phố Đội Cấn, Phường Vĩnh Phúc, Quận Ba Đình, Vĩnh Phúc, Ba Đình, Hà Nội", Image = "Techcombank.webp" },
                    new ATM() { ATMID = 3, Branch = "MilitaryBank", Address = "185, Đường Hoàng Hoa Thám, Phường Liễu Giai, Quận Ba Đình, Thuỵ Khuê, Tây Hồ, Hà Nội", Image = "Militarybank.png" },
                    new ATM() { ATMID = 4, Branch = "TienPhongBank", Address = "152, Đường Thụy Khuê, Phường Thụy Khuê, Quận Tây Hồ, Thuỵ Khuê, Tây Hồ, Hà Nội", Image = "TPBank.webp" },
                    new ATM() { ATMID = 5, Branch = "DongABank", Address = "285 Đội Cấn, Liễu Giai, Ba Đình, Hà Nội", Image = "DongABank.webp" },
                    new ATM() { ATMID = 6, Branch = "SacomBank", Address = "349 Đội Cấn, Liễu Giai, Ba Đình, Hà Nội", Image = "Sacombank.webp" },
                    new ATM() { ATMID = 7, Branch = "SaigonBank", Address = "235 Đ. Lạc Long Quân, Bái  n, Cầu Giấy, Hà Nội", Image = "SaiGonBank.png" },
                    new ATM() { ATMID = 8, Branch = "AgriBank", Address = "3 P. Liễu Giai, Liễu Giai, Ba Đình, Hà Nội", Image = "AGriBank.webp" },
                    new ATM() { ATMID = 9, Branch = "SouthernBank", Address = "Số 39, Phố Đào Tấn, Phường Cống Vị, Quận Ba Đình, Ngọc Khánh, Ba Đình, Hà Nội", Image = "SouthernBank.png" },
                    new ATM() { ATMID = 10, Branch = "CitiBank", Address = "Pullman Hà Nội 40", Image = "CiTiBank.png" }
          );

            modelBuilder.Entity<Stock>().HasData(
                     new Stock() { StockID = 1, Quantity = 200, MoneyID = 1, ATMID = 1 },
                     new Stock() { StockID = 2, Quantity = 200, MoneyID = 2, ATMID = 1 },
                     new Stock() { StockID = 3, Quantity = 200, MoneyID = 3, ATMID = 1 },
                     new Stock() { StockID = 4, Quantity = 200, MoneyID = 4, ATMID = 1 },
                     new Stock() { StockID = 5, Quantity = 400, MoneyID = 1, ATMID = 2 },
                     new Stock() { StockID = 6, Quantity = 200, MoneyID = 2, ATMID = 2 },
                     new Stock() { StockID = 7, Quantity = 200, MoneyID = 1, ATMID = 3 },
                     new Stock() { StockID = 8, Quantity = 200, MoneyID = 2, ATMID = 3 },
                     new Stock() { StockID = 9, Quantity = 500, MoneyID = 3, ATMID = 3 },
                     new Stock() { StockID = 10, Quantity = 400, MoneyID = 1, ATMID = 4 },
                     new Stock() { StockID = 11, Quantity = 400, MoneyID = 2, ATMID = 4 },
                     new Stock() { StockID = 12, Quantity = 400, MoneyID = 3, ATMID = 4 },
                     new Stock() { StockID = 13, Quantity = 400, MoneyID = 4, ATMID = 4 },
                     new Stock() { StockID = 14, Quantity = 400, MoneyID = 1, ATMID = 5 },
                     new Stock() { StockID = 15, Quantity = 400, MoneyID = 2, ATMID = 5 },
                     new Stock() { StockID = 16, Quantity = 400, MoneyID = 1, ATMID = 6 },
                     new Stock() { StockID = 17, Quantity = 400, MoneyID = 2, ATMID = 6 },
                     new Stock() { StockID = 18, Quantity = 400, MoneyID = 1, ATMID = 7 },
                     new Stock() { StockID = 19, Quantity = 400, MoneyID = 2, ATMID = 7 },
                     new Stock() { StockID = 20, Quantity = 400, MoneyID = 1, ATMID = 8 },
                     new Stock() { StockID = 21, Quantity = 400, MoneyID = 2, ATMID = 8 },
                     new Stock() { StockID = 22, Quantity = 400, MoneyID = 1, ATMID = 9 },
                     new Stock() { StockID = 23, Quantity = 400, MoneyID = 2, ATMID = 9 },
                     new Stock() { StockID = 24, Quantity = 400, MoneyID = 1, ATMID = 10 },
                     new Stock() { StockID = 25, Quantity = 400, MoneyID = 2, ATMID = 10 }

         );

            modelBuilder.Entity<LogType>().HasData(
                    new LogType() { LogTypeId = 1, Description = "Withdraw" },
                    new LogType() { LogTypeId = 2, Description = "Transfer" },
                    new LogType() { LogTypeId = 3, Description = "Check balance" },
                    new LogType() { LogTypeId = 4, Description = "Change PIN" }
         );

            modelBuilder.Entity<Log>().HasData(
                    new Log()
                    {
                        LogID = 1,
                        LogDate = new DateTime(2022, 01, 11),
                        Amount = 1000000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 1,
                        CardNo = "9696308315598901"
                    },
                    new Log()
                    {
                        LogID = 2,
                        LogDate = new DateTime(2022, 02, 5),
                        Amount = 2000000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 5,
                        CardNo = "9696308315598901"
                    },
                    new Log()
                    {
                        LogID = 3,
                        LogDate = new DateTime(2022, 06, 24),
                        Amount = 3000000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 4,
                        CardNo = "9696308315598901"
                    },
                    new Log()
                    {
                        LogID = 4,
                        LogDate = new DateTime(2022, 05, 20),
                        Amount = 4000000.00m,
                        Details = "Transfer",
                        LogTypeID = 2,
                        ATMID = 1,
                        CardNo = "2360870160342161"
                    },
                    new Log()
                    {
                        LogID = 5,
                        LogDate = new DateTime(2022, 01, 11),
                        Amount = 500000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 5,
                        CardNo = "2360870160342161"
                    },
                    new Log()
                    {
                        LogID = 6,
                        LogDate = new DateTime(2022, 12, 30),
                        Amount = 6000000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 1,
                        CardNo = "4966575087181020"
                    },
                    new Log()
                    {
                        LogID = 7,
                        LogDate = new DateTime(2022, 10, 30),
                        Amount = 3000000.00m,
                        Details = "Check balance",
                        LogTypeID = 3,
                        ATMID = 7,
                        CardNo = "2836996267348266"
                    },
                    new Log()
                    {
                        LogID = 8,
                        LogDate = new DateTime(2022, 02, 28),
                        Amount = 500000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 2,
                        CardNo = "2836996267348266"
                    },
                    new Log()
                    {
                        LogID = 9,
                        LogDate = new DateTime(2022, 02, 28),
                        Amount = 9000000.00m,
                        Details = "Transfer",
                        LogTypeID = 2,
                        ATMID = 3,
                        CardNo = "5539328606652179"
                    },
                    new Log()
                    {
                        LogID = 10,
                        LogDate = new DateTime(2022, 07, 01),
                        Amount = 1000000.00m,
                        Details = "Check balance",
                        LogTypeID = 3,
                        ATMID = 10,
                        CardNo = "5539328606652179"
                    },
                    new Log()
                    {
                        LogID = 11,
                        LogDate = new DateTime(2022, 03, 28),
                        Amount = 1000000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 3,
                        CardNo = "0993755230042035"
                    },
                    new Log()
                    {
                        LogID = 12,
                        LogDate = new DateTime(2022, 05, 01),
                        Amount = 5000000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 8,
                        CardNo = "8821491318413200"
                    },
                    new Log()
                    {
                        LogID = 13,
                        LogDate = new DateTime(2022, 02, 12),
                        Amount = 1000000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 10,
                        CardNo = "6835760905033599"
                    },
                    new Log()
                    {
                        LogID = 14,
                        LogDate = new DateTime(2022, 05, 12),
                        Amount = 2000000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 7,
                        CardNo = "1682200004184550"
                    },
                    new Log()
                    {
                        LogID = 15,
                        LogDate = new DateTime(2022, 03, 28),
                        Amount = 2000000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 7,
                        CardNo = "1282325404183010"
                    },
                    new Log()
                    {
                        LogID = 16,
                        LogDate = new DateTime(2022, 05, 05),
                        Amount = 5000000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 7,
                        CardNo = "1282325404183010"
                    },
                    new Log()
                    {
                        LogID = 17,
                        LogDate = new DateTime(2022, 02, 08),
                        Amount = 1000000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 7,
                        CardNo = "7084054246868173"
                    },
                    new Log()
                    {
                        LogID = 18,
                        LogDate = new DateTime(2022, 03, 28),
                        Amount = 2000000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 5,
                        CardNo = "9084147846861734"
                    },
                    new Log()
                    {
                        LogID = 19,
                        LogDate = new DateTime(2022, 04, 01),
                        Amount = 2000000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 4,
                        CardNo = "0115755230042035"
                    },
                    new Log()
                    {
                        LogID = 20,
                        LogDate = new DateTime(2022, 04, 12),
                        Amount = 5000000.00m,
                        Details = "Withdraw",
                        LogTypeID = 1,
                        ATMID = 7,
                        CardNo = "0115755230042035"
                    }
         );

            modelBuilder.Entity<Config>().HasData(
                     new Config() { ConfigID = 1, MinWithdraw = 50000.00m, MaxWithdraw = 10000000.00m, DateModified = new DateTime(2019, 02, 28), NumPerPage = 5 }
          );
        }
    }
}
